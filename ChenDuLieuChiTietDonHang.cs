﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace BanDienThoai
{
    public class ChenDuLieuChiTietDonHang
    {
        private ChiTietDH _chitietdonhang;
        public ChiTietDH Chitietdonhang
        {
            get { return _chitietdonhang; }
            set { _chitietdonhang = value; }
        }
        public void chendulieuchitiet()
        {
            SqlDataSource sqldata = new SqlDataSource();
            KetNoiCSDL chuoiketnoi = new KetNoiCSDL();
            sqldata.ConnectionString = chuoiketnoi.GetSetChuoiKetNoi;
            sqldata.InsertCommandType = SqlDataSourceCommandType.StoredProcedure;
            sqldata.InsertCommand = "ChiTietDonHang_Insert";
            sqldata.InsertParameters.Add("IDDonhang", Chitietdonhang.Iddonhang.ToString());
            sqldata.InsertParameters.Add("IDsanpham", Chitietdonhang.Idsanpham.ToString());
            sqldata.InsertParameters.Add("Soluongsanpham", Chitietdonhang.Soluong.ToString());
            sqldata.Insert();
        }
    }
}