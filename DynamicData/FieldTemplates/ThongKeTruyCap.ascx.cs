﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Web.DynamicData;

namespace BanDienThoai.DynamicData.FieldTemplates
{
    public partial class ThongKeTruyCap : System.Web.DynamicData.FieldTemplateUserControl
    {
        KetNoiCSDL KetNoi = new KetNoiCSDL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                lblOnline.Text = Application["SoNguoiOnLine"].ToString();
                SoLuongTruyCap();
            }
        }
        private void SoLuongTruyCap()
        {
            dgvThongKeTruyCap.DataSource = KetNoi.Laydulieu("NguoiTruyCap_Select");
            dgvThongKeTruyCap.DataBind();
        }
    }
}
