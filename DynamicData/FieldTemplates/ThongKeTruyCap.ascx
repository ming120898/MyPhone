﻿<%@ Control Language="C#" CodeBehind="ThongKeTruyCap.ascx.cs" Inherits="BanDienThoai.DynamicData.FieldTemplates.ThongKeTruyCap" %>

<div>
    <table cellpadding="0" cellspacing="0" style="width: 200px"  >
        <tr style="text-align: center;">
            <td style="width: 200px; height: 19px"  >
                <b>Số người Online:</b>
                <asp:Label ID="lblOnline" runat="server" Text="lblOnline"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="1">
                <asp:GridView ID="dgvThongKeTruyCap" runat="server" AutoGenerateColumns="False" Width="200px" GridLines="None" style="text-align: center;">
                    <Columns>
                        <asp:TemplateField HeaderText="Thống kê tổng số truy cập:">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("TongSoTruyCap") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle ForeColor="Black" />
                            <ItemStyle ForeColor="red" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
</div>
