﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BanDienThoai
{
    public partial class DangNhap : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            textUsername.Focus();
        }
        protected void btnDangNhap_Click(object sender, ImageClickEventArgs e)
        {
            if (IsValid)
            {
                NguoiDung nguoidung = new NguoiDung();
                XuLyDangNhapNguoiDung xulydangnhap = new XuLyDangNhapNguoiDung();
                nguoidung.Tendangnhap = textUsername.Text;
                nguoidung.Matkhau = textMatKhau.Text;
                xulydangnhap.Nguoidung = nguoidung;
                xulydangnhap.Thucthi();
                try
                {

                    labelMessage.Text = "Đăng nhập thành công!";
                }
                catch
                {
                    Response.Redirect("TrangLoi.aspx");
                }
                if (xulydangnhap.Dangnhaphople)
                {
                    Application["TenNguoDung"] = nguoidung.Hoten;
                    Application["IdNguoDung"] = nguoidung.Idnguoidung;
                    Label lblWelcome = (Label)Master.FindControl("lblchao");
                    lblWelcome.Text = "Xin chào, " + nguoidung.Hoten;
                    Application["DangNhap"] = 1;
                    Response.Redirect("GioHang.aspx");
                }
                else
                {
                    labelMessage.Text = "Đăng nhập không thành công!";
                }
            }
        }
    }
}