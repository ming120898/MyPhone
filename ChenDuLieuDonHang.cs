﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace BanDienThoai
{
    public class ChenDuLieuDonHang
    {
        private DonHang _donhang;
        public DonHang Donhang
        {
            get { return _donhang; }
            set { _donhang = value; }
        }
        public SqlDataSource chenVaLaydulieu()
        {
            SqlDataSource sqldata = new SqlDataSource();
            KetNoiCSDL chuoiketnoi = new KetNoiCSDL();
            sqldata.ConnectionString = chuoiketnoi.GetSetChuoiKetNoi;
            sqldata.InsertCommandType = SqlDataSourceCommandType.StoredProcedure;
            sqldata.InsertCommand = "DonHang_Insert";
            sqldata.InsertParameters.Add("IdNguoiDung", Donhang.Idnguoidung.ToString());
            sqldata.InsertParameters.Add("IdGiaoDich", Donhang.Idgiaodich.ToString());
            sqldata.Insert();
            sqldata.SelectCommandType = SqlDataSourceCommandType.StoredProcedure;
            sqldata.SelectCommand = "DonHang_Top1_Select "; //mục đích để lấy iddonhang nên chỉ cần lấy 1 dòng là đủ r
            return sqldata;
        }
    }
}