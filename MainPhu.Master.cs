﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BanDienThoai
{
    public partial class MainPhu : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Application["DangNhap"].ToString() == "1")
            {
                btnDangNhap.Text = Application["TenNguoDung"].ToString();
                btnDangXuat.Visible = true;
                btnThongTin.Visible = true;
            }
            else
            {
                btnDangXuat.Visible = false;
                btnThongTin.Visible = false;
            }
        }
        protected void btnDangNhap_Click(object sender, EventArgs e)
        {
            Response.Redirect("DangNhap.aspx");
        }

        protected void btnDangXuat_Click(object sender, EventArgs e)
        {
            //Người dùng không truy cập nữa thì giảm đi 1
            Application["SoNguoiOnLine"] = (int)Application["SoNguoiOnLine"] - 1;
            Application["TenNguoiDung"] = "";
            Application["IdNguoDung"] = "";
            Application["DangNhap"] = 0;
            Response.Redirect("GioiThieuSanPham.aspx");
        }
        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            Response.Redirect("ThongTinCaNhan.aspx");
        }
    }
}