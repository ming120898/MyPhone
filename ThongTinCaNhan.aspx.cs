﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BanDienThoai
{
    public partial class ThongTinCaNhan : System.Web.UI.Page
    {
        //Lớp Kết Nối CSDL
        KetNoiCSDL KN = new KetNoiCSDL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Application["DangNhap"].ToString() == "1")
            {
                LoadLenDataGridView(Application["IdNguoDung"].ToString());
            }
            
        }

        protected void Unnamed1_Click(object sender, EventArgs e)
        {

        }
        public void LoadLenDataGridView(string IdNguoiDung)
        {
            string Lenh = "SELECT * FROM NguoiDung where IdNguoiDung = " +IdNguoiDung;
            dgvKetQua.DataSource = KN.ThucThiLenhTraVeBang(Lenh);
            dgvKetQua.DataBind();
            dgvKetQua.Visible = true;
        }

        protected void Unnamed1_Click1(object sender, EventArgs e)
        {
            Response.Redirect("DoiMatKhau.aspx");
        }
    }
}