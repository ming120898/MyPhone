﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data.SqlClient;

namespace BanDienThoai
{
    public partial class ThemGioHang : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            KetNoiCSDL KetNoi = new KetNoiCSDL();
            KetNoi.MoKetNoi();
            
            int Idsanpham = int.Parse(Request.QueryString["IDsanpham"]);
            string sql = "Select Sum(SoLuong) from SanPham Where Idsanpham = " + Idsanpham;
            SqlCommand cmd = new SqlCommand(sql, KetNoi.LayKetNoi);
            int SoLuongTrongKho = (int)cmd.ExecuteScalar();
            if(SoLuongTrongKho <= 0)
            {
                Session["ThongBaoHetHang"] = "<script>alert('Sản Phẩm Này Đã Hết Hàng, Rất xin lỗi về sự bất tiện này !')</script>";
                Response.Redirect("GioHang.aspx");
                return;
            }
            string Cartguid = CartGUID();
            int Soluong = 1;
            try
            {
                SqlDataSource sqldata = new SqlDataSource();
                string chuoiketnoi = KetNoi.GetSetChuoiKetNoi;
                sqldata.ConnectionString = chuoiketnoi;
                sqldata.InsertCommandType = SqlDataSourceCommandType.StoredProcedure;
                sqldata.InsertCommand = "GioHang_Insert";
                sqldata.InsertParameters.Add("CartGUID", Cartguid);
                sqldata.InsertParameters.Add("IDSanPham", Idsanpham.ToString());
                sqldata.InsertParameters.Add("SoLuong", Soluong.ToString());
                sqldata.Insert();
            }
            catch
            {
                Response.Redirect("Trangloi.aspx");
            }
            Response.Redirect("GioHang.aspx");
        }
        private string CartGUID()
        {
            if (HttpContext.Current.Request.Cookies["DIENTHOAI"] != null)
            {
                return
                HttpContext.Current.Request.Cookies["DIENTHOAI"]["CartID"].ToString();
            }
            else
            {
                Guid CartGUID = Guid.NewGuid();
                HttpCookie cookie = new HttpCookie("DIENTHOAI"); //cookie có tên...
                cookie.Values.Add("CartID", CartGUID.ToString()); //cookie có value ...
                cookie.Expires = DateTime.Now.AddDays(30);          
                HttpContext.Current.Response.AppendCookie(cookie);
                return CartGUID.ToString();
            }
        }
    }
}