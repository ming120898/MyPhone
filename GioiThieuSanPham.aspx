﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainChinh.Master" AutoEventWireup="true" CodeBehind="GioiThieuSanPham.aspx.cs" Inherits="BanDienThoai.GioiThieuSanPham" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:TextBox ID="textSearch" runat="server" CssClass="txtSearch" Height="25px"></asp:TextBox><asp:ImageButton ID="ImageButtonTim" runat="server" ImageUrl="~/images/tim1.jpg" OnClick="ImageButtonTim_Click" Height="26px" ImageAlign="AbsMiddle" /><br />
    <p style="margin-top: 12px;"><asp:Label ID="lblketqua" runat="server" Font-Bold="True" ForeColor="#400000"></asp:Label></p>
      <div>
        <asp:DataList ID="dtlSanPham" runat="server" RepeatColumns="3" Width="530px" CaptionAlign="Top" HorizontalAlign="Center">
            <ItemTemplate>
                <asp:Panel ID="Panel1" runat="server" BorderColor="#E0E0E0" BorderStyle="Solid" BorderWidth="1px" Width="170px" Height="250px">
                <table cellpadding="0" cellspacing="0" style="width:170px;margin-top: 30px;">
                    <tr>
                        <td style="padding: 0px 0px 8px 35px;">
                            <a href='<%# Eval("IdSanPham","ChiTietSanPham.aspx?IdSanpham={0}") %>'>
                                <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("IdHinhSanPham","HienThiHinhSanPham.ashx?IdHinhSanPham={0}") %>' Width="100px" />
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 175px; height: 22px;" align="center">
                            <a href='<%# Eval("IdSanPham","ChiTietSanPham.aspx?IdSanpham={0}") %>' style="text-decoration: none;">
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("TenSanPham") %>' Font-Bold="True" ForeColor="#330000"></asp:Label>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href='<%# Eval("IdSanPham","ChiTietSanPham.aspx?IdSanpham={0}") %>' style="text-decoration: none;">
                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("GiaSanPham", "{0:##,###,###} VND") %>' ForeColor="#330000"></asp:Label>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 4px 0px 0px 0px;">
                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# Eval("IdSanPham","ChiTietSanPham.aspx?IdSanpham={0}") %>' ImageUrl="~/images/button_detail.jpg" style="display:inline-block;width:63px;margin-bottom: 7px;">Chi tiết sản phẩm</asp:HyperLink>
                            <asp:HyperLink ID="HyperLink2" runat="server" ImageUrl="~/images/button_buy.gif" style="display:inline-block;width:80px;padding: 2px 0px 6px 0px;" NavigateUrl='<%# Eval("IdSanPham","ThemGioHang.aspx?IDSanpham={0}") %>'>Thêm vào giỏ hàng</asp:HyperLink>
                        </td>
                    </tr>
                </table>
                </asp:Panel>
            </ItemTemplate>
        </asp:DataList>
    </div>
</asp:Content>
