﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BanDienThoai
{
    public partial class ChiTietSanPham : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Hienchitietsanpham();
            }
        }
        private void Hienchitietsanpham()
        {
            KetNoiCSDL KetNoi = new KetNoiCSDL();
            int Idsanpham = int.Parse(Request.QueryString["IdSanPham"]);
            dtlChiTietSanPham.DataSource = KetNoi.Laydulieu(Idsanpham.ToString(), "SanPhamByID_Select", "IdSanPham");
            dtlChiTietSanPham.DataBind();
        }
    }
}