﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
namespace BanDienThoai
{
    public class XyLyThemDonHang
    {
        private DonHang _donhang;
        public DonHang Donhang
        {
            get { return _donhang; }
            set { _donhang = value; }
        }
        public void Thucthi()
        {
            ChenDuLieuDonHang chendulieudonhang = new ChenDuLieuDonHang(); // chèn vào bảng DonHang
            ChenDuLieuChiTietDonHang chenchitietdonhang = new ChenDuLieuChiTietDonHang(); // chèn vào bảng ChiTietDonHang
            chendulieudonhang.Donhang = this.Donhang;
            GridView g = new GridView();
            g.DataSource = chendulieudonhang.chenVaLaydulieu();
            g.DataBind();
            Donhang.Iddonhang = int.Parse(g.Rows[0].Cells[0].Text);
            chenchitietdonhang.Chitietdonhang = Donhang.Chitietdonhang;
            for (int i = 0; i < Donhang.Chitietdonhang.SP.Length; i++)
            {
                chenchitietdonhang.Chitietdonhang.Iddonhang = Donhang.Iddonhang;
                chenchitietdonhang.Chitietdonhang.Idsanpham = Donhang.Chitietdonhang.SP[i].Idsanpham;
                chenchitietdonhang.Chitietdonhang.Soluong = Donhang.Chitietdonhang.SP[i].Soluong;
                chenchitietdonhang.chendulieuchitiet();
                KetNoiCSDL KetNoi = new KetNoiCSDL();
                KetNoi.MoKetNoi();
                string sql = "Update SanPham set soluong = soluong - "+Donhang.Chitietdonhang.SP[i].Soluong+ " where IdSanPham =" + Donhang.Chitietdonhang.SP[i].Idsanpham;
                SqlCommand cmd = new SqlCommand(sql, KetNoi.LayKetNoi);
                cmd.ExecuteNonQuery();
            }
        }
    }
}