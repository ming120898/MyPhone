﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BanDienThoai
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private DonHang _donhang = new DonHang();
        private decimal _tongtien = 0; // để tính tổng cột thành tiền
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Label lblWelcome = (Label)Master.FindControl("lblChao");
                lblWelcome.Text = "Xin chào, " + Application["TenNguoDung"];
                HienThiGioHang();
            }
        }
        //-----------------Hiển thị nội dung giỏ hàng giống trang GioHang.aspx-----------------
        private void HienThiGioHang()
        {
            GioHang1 giohang = new GioHang1();
            giohang.Cartguid = CartGUID();
            try
            {
                SqlDataSource sqldata = new SqlDataSource();
                KetNoiCSDL chuoiketnoi = new KetNoiCSDL();
                sqldata.ConnectionString = chuoiketnoi.GetSetChuoiKetNoi;
                sqldata.SelectCommandType = SqlDataSourceCommandType.StoredProcedure;
                sqldata.SelectCommand = "GioHang_Select";
                sqldata.SelectParameters.Add("CartGUID", giohang.Cartguid);
                gridgiohang.DataSource = sqldata;
                gridgiohang.DataBind();
            }
            catch
            {
                Response.Redirect("Trangloi.aspx");
            }
        }

        private string CartGUID()
        {
            if (HttpContext.Current.Request.Cookies["DIENTHOAI"] != null)
            {
                return
                HttpContext.Current.Request.Cookies["DIENTHOAI"]["CartID"].ToString();
            }
            else
            {
                Guid CartGUID = Guid.NewGuid();
                HttpCookie cookie = new HttpCookie("DIENTHOAI");
                cookie.Values.Add("CartID", CartGUID.ToString());
                cookie.Expires = DateTime.Now.AddDays(30);
                HttpContext.Current.Response.AppendCookie(cookie);
                return CartGUID.ToString();
            }
        }
        //-------------------Tính tổng cộng của cột thành tiền trong Gridview-------------
        protected void gridgiohang_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                _tongtien += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "ThanhTien"));
            }
            lblTotal.Text = _tongtien.ToString() + " VND";
        }
        //---------Tạo đơn hàng, dựa số liệu trên gridview giỏ hàng-------------------
        private void GuiDonHang()
        {
            XyLyThemDonHang themdonhang = new XyLyThemDonHang();
            themdonhang.Donhang = _donhang;

            try
            {
                themdonhang.Thucthi();
            }
            catch
            {
                Response.Redirect("Trangloi.aspx");
            }
            //Response.Redirect("GioiThieuSanPham.aspx");
        }
        //---------------Sự kiện cho nút tiếp tục mua hàng----------------------------------
        protected void ImageButtonTieptucmuahang_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("GioiThieuSanPham.aspx");
        }
        //---------------Sự kiện cho nút tạo và gửi đơn hàng-----------------

        protected void ImageButtonTaovaguidonhang_Click(object sender, ImageClickEventArgs e)
        {
            DonHang donhang = new DonHang();
            SP[] prods = new SP[gridgiohang.Rows.Count];
            foreach (GridViewRow grow in gridgiohang.Rows)
            {
                if (grow.RowType == DataControlRowType.DataRow)
                {
                    SP Spham = new SP();
                    DataKey data = gridgiohang.DataKeys[grow.DataItemIndex];
                    Spham.Idsanpham = int.Parse(data.Values["IdSanPham"].ToString());
                    Label lblTenSanPham = (Label)grow.FindControl("lblTenSanPham");
                    Spham.Ten = lblTenSanPham.Text;
                    Label lblSoLuong = (Label)grow.FindControl("lblSoLuong");
                    Spham.Soluong = int.Parse(lblSoLuong.Text);
                    Label lblDonGia = (Label)grow.FindControl("lblDonGia");
                    Spham.Giasanpham = Convert.ToInt32(lblDonGia.Text.Replace("VND", ""));
                    prods.SetValue(Spham, grow.DataItemIndex);
                }
            }
            _donhang.Chitietdonhang.SP = prods;
            _donhang.Idnguoidung = (int)Application["IdNguoDung"];
            //Giả lập tạo TransactionID
            _donhang.Idgiaodich = Guid.NewGuid().ToString();
            if (gridgiohang.Rows.Count > 0)
            {
                GuiDonHang();
                Session["XoaGioHang"] = "1";
                Response.Redirect("GioHang.aspx");

            }
            else
            {
                Response.Write("<script>alert('Không thể gửi đơn hàng trống')</script>");
            }
        }
    }
}