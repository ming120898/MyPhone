﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI;

namespace BanDienThoai
{
    /// <summary>
    /// Summary description for HienThiHinhSanPham
    /// </summary>
    public class HienThiHinhSanPham : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            KetNoiCSDL KetNoi = new KetNoiCSDL();
            int Idhinhsanpham = int.Parse(context.Request.QueryString["Idhinhsanpham"]);
            SqlDataSource src = new SqlDataSource();
            src = KetNoi.Laydulieu(Idhinhsanpham.ToString(), "HinhSanpham_Select", "IDHinhSanPham");
            DataView view = (DataView)src.Select(DataSourceSelectArguments.Empty);
            context.Response.BinaryWrite((byte[])view[0]["DuLieuHinhSanPham"]);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}