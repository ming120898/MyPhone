﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.Master" AutoEventWireup="true" CodeBehind="ThemSanPham.aspx.cs" Inherits="BanDienThoai.Admin.ThemSanPham" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="tieude" runat="server">
    <asp:label id="Label1" runat="server" text="THÊM SẢN PHẨM"></asp:label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="phanchinh" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 820px">
        <tr>
            <td style="width: 200px" align="center">
                Tên Sản Phẩm</td>
            <td style="width: 620px">
                <asp:TextBox ID="txtTenSanPham" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTenSanPham"
                    ErrorMessage="Tên sản phẩm không để trống" ForeColor="Red"></asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td style="width: 200px; height: 19px" align="center">
                Danh Mục Sản Phẩm</td>
            <td style="width: 620px; height: 19px">
                <asp:DropDownList ID="dropDanhMucSanPham" runat="server" Width="155px">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width: 200px" align="center">
                Mô Tả Sản Phẩm</td>
            <td style="width: 620px">
                <CKEditor:CKEditorControl ID="CKEditorControlMoTa" runat="server"></CKEditor:CKEditorControl>
            </td>
        </tr>
        <tr>
            <td style="width: 200px" align="center">
                Giá Sản Phẩm</td>
            <td style="width: 620px">
                <asp:TextBox ID="txtGia" runat="server" TextMode="Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtGia"
                    ErrorMessage="Giá sản phẩm không để trống" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 200px" align="center">
                Số lượng</td>
            <td style="width: 620px">
                <asp:TextBox ID="txtSL" runat="server" TextMode="Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtSL"
                    ErrorMessage="Số lượng không để trống" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 200px; height: 22px" align="center">
                Hình Sản Phẩm</td>
            <td style="width: 620px; height: 22px">
                <asp:FileUpload ID="fileuploadHinhSanPham" runat="server" Width="348px"/></td>
        </tr>
        <tr>
            <td style="width: 200px; height: 24px;">
            </td>
            <td style="width: 620px; height: 24px;">
                <asp:Button ID="btnThem" runat="server" Text="Thêm" OnClick="btnThem_Click" CssClass="stylebtn"/>
                <asp:Button ID="btnTroVe" runat="server" Text="Trở về" CausesValidation="False" OnClick="btnTroVe_Click" CssClass="stylebtn" style="background-color: red;" BorderColor="Red"/>
            </td>
        </tr>
        <tr>
            <td style="width: 200px; height: 24px;">
                &nbsp;</td>
            <td style="width: 620px; height: 24px;">
                <asp:Label ID="lblthongbao" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
