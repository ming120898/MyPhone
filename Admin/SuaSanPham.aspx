﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.Master" AutoEventWireup="true" CodeBehind="SuaSanPham.aspx.cs" Inherits="BanDienThoai.Admin.SuaSanPham" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="tieude" runat="server">
    <asp:label id="Label1" runat="server" text="CẬP NHẬT SẢN PHẨM"></asp:label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="phanchinh" runat="server">
    <table style="width: 820px">
        <tr>
            <td style="width: 300px" align="center">
                Tên sản phẩm</td>
            <td style="width: 700px">
                <asp:TextBox ID="txtTenSanPham" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTenSanPham"
                    ErrorMessage="Tên sản phẩm không để trống" ForeColor="Red"></asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td style="width: 300px" align="center">
                Danh mục</td>
            <td style="width: 700px">
                <asp:DropDownList ID="dropDanhMucSanPham" runat="server" Width="156px">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width: 300px" align="center">
                Mô tả sản phẩm</td>
            <td style="width: 700px">
                <CKEditor:CKEditorControl ID="CKEditorControlMoTa" runat="server"></CKEditor:CKEditorControl>
                </td>
        </tr>
        <tr>
            <td style="width: 300px" align="center">
                Đơn giá</td>
            <td style="width: 700px">
                <asp:TextBox ID="txtGia" runat="server" TextMode="Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtGia"
                    ErrorMessage="Giá sản phẩm không để trống" ForeColor="Red"></asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td style="width: 300px" align="center">
                Số lượng</td>
            <td style="width: 700px">
                <asp:TextBox ID="txtSL" runat="server" TextMode="Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtSL"
                    ErrorMessage="Số lượng không để trống" ForeColor="Red"></asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td style="width: 300px" align="center">
                Hình</td>
            <td style="width: 700px">
                <asp:Image ID="imgHinhSanPham" runat="server" Height="125px" Width="100px" /></td>
        </tr>
        <tr>
            <td style="width: 300px">
            </td>
            <td style="width: 700px">
                <asp:FileUpload ID="fileuploadHinhSanPham" runat="server" /></td>
        </tr>
        <tr>
            <td style="width: 300px">
            </td>
            <td style="width: 700px">
                <asp:Button ID="btnCapNhat" runat="server" Text="Cập Nhật" OnClick="btnCapNhat_Click" CssClass="stylebtn"/>
                <asp:Button ID="btnTroVe" runat="server" Text="Trở về" OnClick="btnTroVe_Click" CausesValidation="False" CssClass="stylebtn" style="background-color: red;" BorderColor="Red" /></td>
        </tr>
        <tr>
            <td style="width: 300px">
                &nbsp;</td>
            <td style="width: 700px">
                <asp:Label ID="lblThongBao" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
