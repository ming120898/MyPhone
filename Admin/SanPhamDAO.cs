﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI.WebControls;

namespace BanDienThoai
{
    public class SanPhamDAO
    {
        public bool ThemSanPham(SP Sanpham)
        {
            KetNoi chuoiketnoi = new KetNoi();
            SqlConnection cnn = new SqlConnection(chuoiketnoi.ConnectionString());
            cnn.Open();
            string sql = "DECLARE @IDHinhSanPham int INSERT INTO HinhSanPham (DuLieuHinhSanPham) VALUES (@DuLieuHinhSanPham) SET @IDHinhSanPham = @@IDENTITY INSERT INTO SanPham(IDDanhMucSanPham, TenSanPham, IDHinhSanPham, MoTaSanPham, GiaSanPham) VALUES(@IDDanhMucSanPham, @TenSanPham, @IDHinhSanPham, @MoTaSanPham, @GiaSanPham)";
            SqlCommand cmd = new SqlCommand(sql, cnn);
            cmd.Parameters.AddWithValue("@dulieuhinhsanpham", Sanpham.Dulieuhinhsanpham);
            cmd.Parameters.AddWithValue("@TenSanPham", Sanpham.Ten);
            cmd.Parameters.AddWithValue("@IDDanhMucSanPham", Sanpham.Iddanhmucsanpham);
            cmd.Parameters.AddWithValue("@MoTaSanPham", Sanpham.Mota);
            cmd.Parameters.AddWithValue("@GiaSanPham", Sanpham.Giasanpham);
            int result= (int)cmd.ExecuteNonQuery();
            return (result >= 1);
        }
        public bool SuaSanPham(SP Sanpham)
        {
            KetNoi chuoiketnoi = new KetNoi();
            SqlConnection cnn = new SqlConnection(chuoiketnoi.ConnectionString());
            cnn.Open();
            string sql = "UPDATE HinhSanPham SET DuLieuHinhSanPham = @DuLieuHinhSanPham WHERE IDHinhSanPham = @IDHinhSanPham; UPDATE SanPham SET IDDanhMucSanPham = @IDDanhMucSanPham, TenSanPham = @TenSanPham, IDHinhSanPham = @IDHinhSanPham, MoTaSanPham = @MoTaSanPham, GiaSanPham = @GiaSanPham WHERE IDSanPham = @IDSanPham";
            SqlCommand cmd = new SqlCommand(sql, cnn);
            cmd.Parameters.AddWithValue("@IdDanhMucSanPham", Sanpham.Iddanhmucsanpham);
            cmd.Parameters.AddWithValue("@TenSanPham", Sanpham.Ten);
            cmd.Parameters.AddWithValue("@IdHinhSanPham", Sanpham.Idhinhsanpham);
            cmd.Parameters.AddWithValue("@DuLieuHinhSanPham", Sanpham.Dulieuhinhsanpham);
            cmd.Parameters.AddWithValue("@MoTaSanPham", Sanpham.Mota);
            cmd.Parameters.AddWithValue("@GiaSanPham", Sanpham.Giasanpham);
            cmd.Parameters.AddWithValue("@IdSanPham ", Sanpham.Idsanpham);
            int result = (int)cmd.ExecuteNonQuery();
            return (result >= 1);
        }
        public bool XoaSanPham(int id, int idhinh)
        {
            KetNoi chuoiketnoi = new KetNoi();
            SqlConnection cnn = new SqlConnection(chuoiketnoi.ConnectionString());
            cnn.Open();
            string sql = "DELETE FROM SanPham WHERE IdSanPham = '" + id + "' ; DELETE FROM HinhSanPham WHERE IdHinhSanPham = '" + idhinh + "'";
            SqlCommand cmd= new SqlCommand(sql, cnn);
            int result = (int)cmd.ExecuteNonQuery();
            return (result >= 1);
        }
        public SqlDataSource LaySanPham()
        {
            KetNoi chuoiketnoi = new KetNoi();
            string sql= "SELECT IDSanPham,TenSanPham,TenDanhMucSanPham,IDHinhSanPham, SUBSTRING(MoTaSanPham, 1, 150) + '...' AS MoTaSanPham, GiaSanPham FROM SanPham INNER JOIN DanhMucSanPham ON DanhMucSanPham.IDDanhMucSanPham = SanPham.IDDanhMucSanPham";
            SqlDataSource data = new SqlDataSource(chuoiketnoi.ConnectionString(), sql);
            return data;
        }
        public SqlDataSource LayHinhAnh(int ID)
        {
            KetNoi chuoiketnoi = new KetNoi();
            string sql = "SELECT DuLieuHinhSanPham FROM HinhSanPham WHERE IDHinhSanPham = '"+ID+"'";
            SqlDataSource data = new SqlDataSource(chuoiketnoi.ConnectionString(), sql);
            return data;
        }
        public SP LaySanPhamByID(int id)
        {
            KetNoi chuoiketnoi = new KetNoi();
            string sql = "SELECT IDSanPham,TenSanPham,TenDanhMucSanPham,IDHinhSanPham,MoTaSanPham,GiaSanPham FROM SanPham INNER JOIN DanhMucSanPham ON DanhMucSanPham.IDDanhMucSanPham = SanPham.IDDanhMucSanPham WHERE IDSanPham = '"+id+"'";
            SqlDataSource data = new SqlDataSource(chuoiketnoi.ConnectionString(), sql);
            SP Sanpham = new SP();
            GridView grid = new GridView();
            grid.DataSource = data;
            grid.DataBind();
            Sanpham.Ten = grid.Rows[0].Cells[1].Text.ToString();
            Sanpham.Mota = grid.Rows[0].Cells[4].Text.ToString();
            Sanpham.Giasanpham = Convert.ToInt32(grid.Rows[0].Cells[5].Text.ToString());
            Sanpham.Idsanpham = int.Parse(grid.Rows[0].Cells[0].Text.ToString());
            Sanpham.Danhmucsanpham.Tendanhmucsanpham = grid.Rows[0].Cells[2].Text.ToString();
            Sanpham.Idhinhsanpham = int.Parse(grid.Rows[0].Cells[3].Text.ToString());
            return Sanpham;
        }
    }
}