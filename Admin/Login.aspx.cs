﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BanDienThoai.Admin
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["login"] != null)
            {
                Response.Redirect("Default.aspx");
            }
        }

        protected void btnDangNhap_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                NguoiDung nguoidung = new NguoiDung();
                XuLyDangNhapAdmin xulydangnhapadmin = new XuLyDangNhapAdmin();
                nguoidung.Tendangnhap = textUsername.Text;
                nguoidung.Matkhau = textMatKhau.Text;
                xulydangnhapadmin.Nguoidung = nguoidung;
                xulydangnhapadmin.Thucthi();
                if (xulydangnhapadmin.Dangnhaphople)
                {
                    Session["login"] = nguoidung.Hoten;
                    Response.Redirect("Default.aspx");
                }
                else
                {
                    lblthongbao.Text = "Đăng nhập không thành công!";
                }           
            }
        }

        protected void ckbhien_CheckedChanged(object sender, EventArgs e)
        {
            string pass = textMatKhau.Text;
            if (ckbhien.Checked)
            {
                textMatKhau.TextMode = TextBoxMode.SingleLine;
                textMatKhau.Text = pass;
            }
            else
            {
                textMatKhau.TextMode = TextBoxMode.Password;
                textMatKhau.Text = pass;
            }
        }
    }
}