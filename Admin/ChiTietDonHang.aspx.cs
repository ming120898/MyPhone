﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BanDienThoai
{
    public partial class ChiTietDonHang : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["IDdonhang"] == null)
                Response.Redirect("ThongKeDonHang.aspx");

            if (!IsPostBack)
            {
                HienThiTinhTrangDonHang();
                HienThiChiTietDonHang();
            }
        }
        // ------------Hiển thị chi tiết đơn hàng trong gridview----------
        private void HienThiChiTietDonHang()
        {
            DonHang donhang = new DonHang();

            SqlDataSource sqldata = new SqlDataSource();
            KetNoiCSDL chuoiketnoi = new KetNoiCSDL();
            sqldata.ConnectionString = chuoiketnoi.GetSetChuoiKetNoi;
            sqldata.SelectCommandType = SqlDataSourceCommandType.StoredProcedure;
            sqldata.SelectCommand = "DonHangByID_Select";
            sqldata.SelectParameters.Add("IDDonHang", int.Parse(Request.QueryString["IDdonhang"]).ToString());
            donhang.Iddonhang = int.Parse(Request.QueryString["IDdonhang"]);
            GridView grid = new GridView();
            grid.DataSource = sqldata;
            grid.DataBind();
            if (grid.Rows.Count > 0)
            {
                if (grid.Rows[0].Cells[1].Text.ToString() != "&nbsp;")
                //grid.Rows[0].Cells[1]phu thuoc cau truy van, lay cot ngay xu ly don hang
                {
                    donhang.Ngayxulydonhang =
                    Convert.ToDateTime(grid.Rows[0].Cells[1].Text.ToString());
                }
                donhang.Trackingnumber =
                grid.Rows[0].Cells[3].Text.ToString().Replace("&nbsp;", "");
                donhang.Idtinhtrangdonhang = int.Parse(grid.Rows[0].Cells[2].Text.ToString());

            }
            chuoiketnoi.DongKetNoi();
            SqlDataSource sqldata1 = new SqlDataSource();
            KetNoiCSDL chuoiketnoi1 = new KetNoiCSDL();
            sqldata1.ConnectionString = chuoiketnoi1.GetSetChuoiKetNoi;
            sqldata1.SelectCommandType = SqlDataSourceCommandType.StoredProcedure;
            sqldata1.SelectCommand = "ChiTietDonHang_Select";
            sqldata1.SelectParameters.Add("IDDonHang", int.Parse(Request.QueryString["IDdonhang"]).ToString());
            gridviewOrderDetailsProducts.DataSource = sqldata1;
            gridviewOrderDetailsProducts.DataBind();
            
            //------Hiển thị ID giao dịch trong label------------------
            labelTransactionID.Text = Request.QueryString["IDgiaodich"];
            //------Hiển thị ngày xử lý đơn hàng---------------------
            if (donhang.Ngayxulydonhang != DateTime.MinValue)
            {
                textShippedDate.Text = donhang.Ngayxulydonhang.ToShortDateString();
            }
            //--------Hiển thị giá trị Trackingnumber trong textbox---------
            textTrackingNumber.Text = donhang.Trackingnumber;
            //-------Lấy dữ liệu tình trạng đơn hàng trong dropdowlist-------
            dropdownlistOrderStatus.SelectedIndex =
            dropdownlistOrderStatus.Items.IndexOf(dropdownlistOrderStatus.Items.FindByValue(donhang.Idtinhtrangdonhang.ToString()));
        }
        // Hiển thị tình trạng đơn hàng trong dropdownlist-------
        private void HienThiTinhTrangDonHang()
        {
            SqlDataSource sqldata = new SqlDataSource();
            try
            {
                KetNoiCSDL chuoiketnoi = new KetNoiCSDL();
                sqldata.ConnectionString = chuoiketnoi.GetSetChuoiKetNoi;
                sqldata.SelectCommandType = SqlDataSourceCommandType.StoredProcedure;
                sqldata.SelectCommand = "TinhTrangDonHang_Select";
            }
            catch
            {
                Response.Redirect("../Trangloi.aspx");
            }
            dropdownlistOrderStatus.DataTextField = "TenTinhTrangDonHang";
            dropdownlistOrderStatus.DataValueField = "IDTinhTrangDonHang";
            dropdownlistOrderStatus.DataSource = sqldata;
            dropdownlistOrderStatus.DataBind();
        }
        //---------Xự kiện nút trở về--------------------
        protected void btnTroVe_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("ThongKeDonHang.aspx");
        }
        // ---------sự kiện kích image button--------------

        protected void imagebuttonDatePicker_Click(object sender, ImageClickEventArgs e)
        {
            if (calendarDatePicker.Visible)
            {
                calendarDatePicker.Visible = false;
            }
            else
            {
                calendarDatePicker.Visible = true;
            }
        }
        //----------Sự kiện chọn giá trị trên điều khiển Calenda------
        protected void calendarDatePicker_SelectionChanged(object sender, EventArgs e)
        {
            textShippedDate.Text = calendarDatePicker.SelectedDate.ToShortDateString();
            calendarDatePicker.Visible = false;
        }
        // --Xử lý nút cập nhật để cập nhật thay đổi đơn hàng sau khi xử lý-------

        protected void btnCapNhat_Click(object sender, ImageClickEventArgs e)
        {
            DonHang donhang = new DonHang();
            donhang.Iddonhang = int.Parse(Request.QueryString["Iddonhang"]);
            donhang.Idtinhtrangdonhang =
            int.Parse(dropdownlistOrderStatus.SelectedItem.Value);
            donhang.Ngayxulydonhang = Convert.ToDateTime(textShippedDate.Text);
            donhang.Trackingnumber = textTrackingNumber.Text;
            
            try
            {
                SqlDataSource sqldata = new SqlDataSource();
                KetNoiCSDL chuoiketnoi = new KetNoiCSDL();
                sqldata.ConnectionString = chuoiketnoi.GetSetChuoiKetNoi;
                sqldata.UpdateCommandType = SqlDataSourceCommandType.StoredProcedure;
                sqldata.UpdateCommand = "DonHang_Update";
                sqldata.UpdateParameters.Add("IDDonHang",donhang.Iddonhang.ToString());
                sqldata.UpdateParameters.Add("IDTinhTrangDonHang", donhang.Idtinhtrangdonhang.ToString());
                sqldata.UpdateParameters.Add("NgayXuLyDonHang", donhang.Ngayxulydonhang.ToShortDateString());
                sqldata.UpdateParameters.Add("TrackingNumber", donhang.Trackingnumber.ToString());
                sqldata.Update();
            }
            catch
            {
                Response.Redirect("../Trangloi.aspx");
            }
            Response.Redirect("ThongKeDonHang.aspx");
        }
    }
}