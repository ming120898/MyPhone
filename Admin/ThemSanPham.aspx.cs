﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace BanDienThoai.Admin
{
    public partial class ThemSanPham : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtTenSanPham.Focus(); // txtTenSanPham là ID của TextBox
                HienThiDanhMucSanPham();
            }
        }
        private void HienThiDanhMucSanPham()
        {
            SqlDataSource sqldata = new SqlDataSource();
            try
            {
                KetNoiCSDL chuoiketnoi = new KetNoiCSDL();
                sqldata.ConnectionString = chuoiketnoi.GetSetChuoiKetNoi;
                sqldata.SelectCommandType = SqlDataSourceCommandType.StoredProcedure;
                sqldata.SelectCommand = "DanhMucSanPham_Select";
            }
            catch
            {
                Response.Redirect("../Trangloi.aspx");
            }
            dropDanhMucSanPham.DataTextField = "TenDanhMucSanPham";
            dropDanhMucSanPham.DataValueField = "IDDanhMucSanPham";
            dropDanhMucSanPham.DataSource = sqldata;
            dropDanhMucSanPham.DataBind();
        }

        protected void btnThem_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                SP Sanpham = new SP
                {
                    Iddanhmucsanpham = int.Parse(dropDanhMucSanPham.SelectedItem.Value),
                    Ten = txtTenSanPham.Text, // txtTenSanPham là ID của TextBox
                    Mota = CKEditorControlMoTa.Text,//txtTenSanPham là ID của TextBox
                    Dulieuhinhsanpham = fileuploadHinhSanPham.FileBytes,
                    // fileuploadHinhSanPham là ID của điều khiển FileUpLoad
                    Giasanpham = Convert.ToDecimal(txtGia.Text), // txtGia là ID của TextBox
                    Soluong = int.Parse(txtSL.Text)
                };

                try
                {
                    KetNoiCSDL chuoiketnoi = new KetNoiCSDL();
                    SqlConnection conect = new SqlConnection(chuoiketnoi.GetSetChuoiKetNoi);
                    conect.Open();
                    SqlCommand com = new SqlCommand();
                    com.Connection = conect;
                    com.CommandType = CommandType.StoredProcedure;
                    com.CommandText = "SanPham_Insert";
                    com.Parameters.Add("@dulieuhinhsanpham", SqlDbType.Image).Value = Sanpham.Dulieuhinhsanpham;
                    com.Parameters.Add("@TenSanPham", SqlDbType.NVarChar).Value = Sanpham.Ten;
                    com.Parameters.Add("@IDDanhMucSanPham", SqlDbType.Int).Value = Sanpham.Iddanhmucsanpham;
                    com.Parameters.Add("@MoTaSanPham", SqlDbType.NVarChar).Value = Sanpham.Mota;
                    com.Parameters.Add("@GiaSanPham", SqlDbType.Int).Value = Sanpham.Giasanpham;
                    com.Parameters.Add("@SoLuong", SqlDbType.Int).Value = Sanpham.Soluong;
                    com.ExecuteNonQuery();

                    lblthongbao.Text = "Thêm thành công.";
                }
                catch
                {
                    Response.Redirect("../Trangloi.aspx");
                }
                
            }
        }

        protected void btnTroVe_Click(object sender, EventArgs e)
        {
            Response.Redirect("SanPham.aspx");
        }
    }
}