﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace BanDienThoai.Admin
{
    public class TruyVanDuLieuDangNhapAdmin
    {
        private NguoiDung _nguoidung;
        public NguoiDung Nguoidung
        {
            get { return _nguoidung; }
            set { _nguoidung = value; }
        }
        public SqlDataSource Laydulieu()
        {
            SqlDataSource sqldata = new SqlDataSource();
            KetNoi chuoiketnoi = new KetNoi();
            sqldata.ConnectionString = chuoiketnoi.ConnectionString();
            sqldata.SelectCommandType = SqlDataSourceCommandType.StoredProcedure;
            sqldata.SelectCommand = "DangNhapAdmin_Select";
            sqldata.SelectParameters.Add("TenDangNhap", Nguoidung.Tendangnhap);
            sqldata.SelectParameters.Add("MatKhau", Nguoidung.Matkhau);
            return sqldata;
        }
    }
}