﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.Master" AutoEventWireup="true" CodeBehind="TongQuan.aspx.cs" Inherits="BanDienThoai.Admin.TongQuan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="tieude" runat="server">
    <asp:Label ID="Label1" runat="server" Text="TỔNG QUAN HỆ THỐNG"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="phanchinh" runat="server">
    <table style="width:100%;font-size: 20px;" border="1" frame="above">
        <tr>
            <td colspan="2" bgcolor="#FFCC99">
                <center><asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label></center>
            </td>
            <td colspan="2" bgcolor="#CCCCFF">
                <center><asp:Label ID="Label3" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label></center>
            </td>
        </tr>
        <tr>
            <td style="width: 219px; height: 25px;" bgcolor="#FFCC99">
                <asp:Label ID="Label5" runat="server" Text="Tổng số đơn hàng đã đặt:"></asp:Label>
            </td>
            <td style="width: 217px; height: 25px;" bgcolor="#FFCC99">
                <asp:Label ID="Label15" runat="server"></asp:Label>
            </td>
            <td style="width: 238px; height: 25px;" bgcolor="#CCCCFF">
                <asp:Label ID="Label12" runat="server" Text="Tổng số đơn hàng đã đặt:"></asp:Label>
            </td>
            <td bgcolor="#CCCCFF" style="height: 25px">
                <asp:Label ID="Label20" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 219px" bgcolor="#FFCC99">
                <asp:Label ID="Label6" runat="server" Text="Tổng sổ sản phẩm đã đặt:"></asp:Label>
            </td>
            <td style="width: 217px" bgcolor="#FFCC99">
                <asp:Label ID="Label16" runat="server"></asp:Label>
            </td>
            <td style="width: 238px" bgcolor="#CCCCFF">
                <asp:Label ID="Label13" runat="server" Text="Tổng sổ sản phẩm đã đặt:"></asp:Label>
            </td>
            <td bgcolor="#CCCCFF">
                <asp:Label ID="Label21" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 219px" bgcolor="#FFCC99">
                <asp:Label ID="Label10" runat="server" Text="Tổng doanh thu:"></asp:Label>
            </td>
            <td style="width: 217px" bgcolor="#FFCC99">
                <asp:Label ID="Label18" runat="server"></asp:Label>
            </td>
            <td style="width: 238px" bgcolor="#CCCCFF">
                <asp:Label ID="Label14" runat="server" Text="Tổng doanh thu:"></asp:Label>
            </td>
            <td bgcolor="#CCCCFF">
                <asp:Label ID="Label22" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="4" bgcolor="#CCFFCC">
                <asp:Label ID="Label23" runat="server" Text="Tổng số lượng truy cập website:"></asp:Label>
&nbsp;<asp:Label ID="Label24" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
