﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="BanDienThoai.Admin.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Đăng nhập quản trị trang web</title>
    <style>
        .stylebtn {
            background: #288ad6;
            position: relative;
            line-height: 28px;
            font-size: 14px;
            color: #fff;
            border: 1px solid #288ad6;
            border-radius: 4px;
            margin-right: 5px;
            padding: 0 3.3%;
            text-align: center;
            cursor: pointer;
        }

        .left_box {
            background: url(/images/left_box.png) no-repeat;
            width: 10px;
            height: 33px;
            float: left;
        }

        .right_box {
            background: url(/images/right_box.png) no-repeat;
            width: 10px;
            height: 33px;
            float: left;
        }

        .bottombox_left {
            background: url(/images/bottombox_left.gif) no-repeat;
            width: 10px;
            height: 6px;
            float: left;
        }

        .bottombox_center {
            background: url(/images/bottombox_center.gif) repeat-x;
            height: 6px;
            float: left;
        }

        .bottombox_right {
            background: url(/images/bottombox_right.gif) no-repeat;
            width: 10px;
            height: 6px;
            float: left;
        }

        .box {
            background: url(/images/box.jpg) repeat-x;
            width: 288px;
            float: left;
            height: 33px;
        }

        .title_box {
            font-family: Tahoma, Geneva, sans-serif;
            color: #ffffff;
            font-weight: bold;
            font-size: 15px !important;
            padding-top: 7px !important;
        }
        .auto-style1 {
            width: 37%;
            height: 15px;
        }
        .auto-style2 {
            width: 60%;
            height: 15px;
        }
        .auto-style3 {
            height: 14px;
        }
        .auto-style4 {
            height: 23px;
        }
        .auto-style5 {
            float: left;
            width: 414px;
        }
        .auto-style6 {
            width: 170px;
            margin-left: 0px;
        }
        .auto-style7 {
            width: 37%;
            height: 33px;
        }
        .auto-style8 {
            width: 60%;
            height: 33px;
        }
    </style>
</head>
<body>
    <div align="center">
        <div style="width: 408px; margin-right: 12px;">
            <div class="left_box">
            </div>
            <div style="width: 388px;" class="box">
                <div class="title_box" align="center">
                    Đăng nhập Quản Trị
                </div>
            </div>
            <div class="right_box">
            </div>
            <div style="background-color: #edeeef; padding-top: 19px; padding-bottom: 19px;" class="auto-style5">
                <form id="form1" runat ="server">
                    <table id="Login1" cellspacing="0" cellpadding="0" border="0" style="width: 408px; border-collapse: collapse;">
                        <tr>
                            <td>
                                <table style="width: 100%;" align="center" cellpadding="4px;">
                                    <tr>
                                        <td class="auto-style1" style="padding-bottom: 24px;" align="right">Tên đăng nhập:
                                        </td>
                                        <td align="left" class="auto-style2">
                                            <asp:TextBox ID="textUsername" runat="server" CssClass="auto-style6"></asp:TextBox>
                                            <br />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Tên đăng nhập không được trống" Text="Tên đăng nhập không được trống" ControlToValidate="textUsername" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 22px;" class="auto-style7" align="right">Mật khẩu:
                                        </td>
                                        <td align="left" class="auto-style8">
                                            <asp:TextBox ID="textMatKhau" runat="server" class="login-pw" Style="width: 170px;" TextMode="Password"></asp:TextBox>
                                            <br />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Mật khẩu không được để trống" Text="Mật khẩu không được để trống" ForeColor="Red" ControlToValidate="textMatKhau"></asp:RequiredFieldValidator>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center" class="auto-style3">
                                            <asp:CheckBox ID="ckbhien" runat="server" AutoPostBack="True" OnCheckedChanged="ckbhien_CheckedChanged" /><a class="textlink">Hiện mật khẩu</a>
                                        </td>
                                    </tr>
                                    <tr align="center">
                                        <td colspan="2" class="auto-style4">
                                            &nbsp;<asp:Label ID="lblthongbao" runat="server" ForeColor="Red"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <asp:Button ID="btnDangNhap" runat="server" Text="Đăng nhập" class="stylebtn" OnClick="btnDangNhap_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            <div class="bottombox_left">
                &nbsp;
            </div>
            <div style="width: 388px;" class="bottombox_center">
                &nbsp;
            </div>
            <div class="bottombox_right">
                &nbsp;
            </div>
        </div>
    </div>
</body>
</html>
