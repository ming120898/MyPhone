﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.Master" AutoEventWireup="true" CodeBehind="DanhMuc.aspx.cs" Inherits="BanDienThoai.Admin.DanhMuc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="tieude" runat="server">
    <asp:label id="Label1" runat="server" text="QUẢN LÝ DANH MỤC"></asp:label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="phanchinh" runat="server">
    <div class="menuright">
        <asp:Button ID="btnThemDanhMuc" runat="server" Text="Thêm danh mục" OnClick="btnThemDanhMuc_Click" CssClass="stylebtn" />
        <asp:Button ID="btnXoa" runat="server" Text="Xóa" CssClass="stylebtn" style="background-color: red;" BorderColor="Red" OnClick="btnXoa_Click" OnClientClick="return confirm('Bạn có thực sự muốn xóa không?')"/>
        <br />
        <br />
        <asp:Label ID="lblThongBao" runat="server" ForeColor="Red"></asp:Label>
    </div>
    <center>
        <asp:GridView ID="grvDanhMuc" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Vertical" AllowPaging="True" OnPageIndexChanging="grvDanhMuc_PageIndexChanging">
            <AlternatingRowStyle BackColor="#CCCCCC" />
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:CheckBox ID="ckbChon" runat="server" />
                    </ItemTemplate>
                    <HeaderStyle BackColor="#E0E0E0" ForeColor="Maroon" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="STT">
                    <ItemTemplate><%# get_stt() %></ItemTemplate>
                    <HeaderStyle BackColor="#E0E0E0" ForeColor="Maroon" />
                </asp:TemplateField>
                <asp:BoundField HeaderText="ID Danh mục" DataField="IdDanhMucSanPham">
                    <HeaderStyle BackColor="#E0E0E0" ForeColor="Maroon" />
                </asp:BoundField>
                <asp:HyperLinkField DataNavigateUrlFields="IdDanhMucSanPham" DataNavigateUrlFormatString="SuaDanhMuc.aspx?IdDanhMucSanPham={0}" DataTextField="TenDanhMucSanPham" HeaderText="Tên Danh Mục">
                    <HeaderStyle BackColor="#E0E0E0" ForeColor="Maroon" />
                </asp:HyperLinkField>
            </Columns>
            <EmptyDataTemplate>
                <asp:CheckBox ID="ckbChon" runat="server" />
            </EmptyDataTemplate>
            <Footerstyle backcolor="#cccccc" />
            <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#808080" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#383838" />
        </asp:GridView>
    </center>
</asp:Content>
