﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace BanDienThoai.Admin
{
    public partial class SuaDanhMuc : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["IdDanhMucSanPham"] == null)
                Response.Redirect("DanhMuc.aspx");
            if (!IsPostBack)
            {
                HienTenDanhMuc();
                txtTenDanhMuc.Focus();
            }
        }

        private void HienTenDanhMuc()
        {
            DanhMucSanPham dmsp = new DanhMucSanPham();

            SqlDataSource sqldata = new SqlDataSource();
            KetNoiCSDL chuoiketnoi = new KetNoiCSDL();
            sqldata.ConnectionString = chuoiketnoi.GetSetChuoiKetNoi;
            sqldata.SelectCommandType = SqlDataSourceCommandType.StoredProcedure;
            sqldata.SelectCommand = "DanhMucSanPhamByID_Select";
            sqldata.SelectParameters.Add("IdDanhMucSanPham", Request.QueryString["IdDanhMucSanPham"]).ToString();

            GridView grid = new GridView();
            grid.DataSource = sqldata;
            grid.DataBind();
            dmsp.Tendanhmucsanpham = grid.Rows[0].Cells[0].Text.ToString();

            try
            {
                txtTenDanhMuc.Text = dmsp.Tendanhmucsanpham;
            }
            catch
            {
                Response.Redirect("../Trangloi.aspx");
            }
        }

        protected void btnCapNhat_Click(object sender, EventArgs e)
        {
            KetNoiCSDL chuoiketnoi = new KetNoiCSDL();
            if (IsValid)
            {
                int Iddanhmucsanpham = int.Parse(Request.QueryString["IdDanhMucSanPham"]);
                try
                {
                    SqlConnection conect = new SqlConnection(chuoiketnoi.GetSetChuoiKetNoi);
                    conect.Open();
                    SqlCommand com = new SqlCommand();
                    com.Connection = conect;
                    com.CommandType = CommandType.StoredProcedure;
                    com.CommandText = "DanhMucSanPham_UpDate";
                    com.Parameters.Add("@IdDanhMucSanPham", SqlDbType.Int).Value = Iddanhmucsanpham;
                    com.Parameters.Add("@TenDanhMucSanPham", SqlDbType.NVarChar).Value = txtTenDanhMuc.Text;
                    com.ExecuteNonQuery();
                }
                catch
                {
                    Response.Redirect("../Trangloi.aspx");
                }
                Response.Redirect("DanhMuc.aspx");
            }
        }

        protected void btnTroVe_Click(object sender, EventArgs e)
        {
            Response.Redirect("DanhMuc.aspx");
        }
    }
}