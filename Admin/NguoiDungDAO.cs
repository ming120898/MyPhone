﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace BanDienThoai
{
    public class NguoiDungDAO
    {
        string st = ConfigurationManager.ConnectionStrings["SQLCONN"].ConnectionString;
        public bool CheckAdmin(string user, string pass)
        {
            SqlConnection cnn = new SqlConnection(st);
            string sql = @"select count(*) from NguoiDung where TenDangNhap = @user and MatKhau = @pass and IDKieuNguoiDung = 2";
            cnn.Open();
            SqlCommand cmd = new SqlCommand(sql, cnn);
            cmd.Parameters.AddWithValue("@user", user);
            cmd.Parameters.AddWithValue("@pass", pass);
            int result = (int)cmd.ExecuteScalar();
            return (result >= 1);
        }
    }
}