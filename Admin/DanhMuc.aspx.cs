﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace BanDienThoai.Admin
{
    public partial class DanhMuc : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                HienThiDanhMuc();
            }
        }
        private void HienThiDanhMuc()
        {
            SqlDataSource sqldata = new SqlDataSource();
            try
            {
                KetNoiCSDL chuoiketnoi = new KetNoiCSDL();
                sqldata.ConnectionString = chuoiketnoi.GetSetChuoiKetNoi;
                sqldata.SelectCommandType = SqlDataSourceCommandType.StoredProcedure;
                sqldata.SelectCommand = "DanhMucSanPham_Select";
            }
            catch
            {
                Response.Redirect("../Trangloi.aspx");
            }
            grvDanhMuc.DataSource = sqldata;
            grvDanhMuc.DataBind();
        }
        protected void btnThemDanhMuc_Click(object sender, EventArgs e)
        {
            Response.Redirect("ThemDanhMuc.aspx");
        }

        int stt = 1;
        protected string get_stt()
        {
            return Convert.ToString(stt++);
        }
        int tranght = 1;
        protected void grvDanhMuc_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvDanhMuc.PageIndex = e.NewPageIndex;   //trang hien tai
            tranght = e.NewPageIndex;
            int trang_thu = e.NewPageIndex;      //the hien trang thu may
            int so_dong = grvDanhMuc.PageSize;       //moi trang co bao nhieu dong
            stt = trang_thu * so_dong + 1;
            HienThiDanhMuc();
        }
        protected void btnXoa_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in grvDanhMuc.Rows)
            {
                CheckBox ck = (CheckBox)row.FindControl("ckbChon");
                if (ck != null && ck.Checked)
                {
                    try
                    {
                        int iddm = int.Parse(row.Cells[2].Text);
                        KetNoiCSDL chuoiketnoi = new KetNoiCSDL();
                        chuoiketnoi.MoKetNoi();
                        string sql = "DELETE FROM SanPham WHERE IdDanhMucSanPham = '" + iddm + "' ; DELETE FROM DanhMucSanPham WHERE IdDanhMucSanPham = '" + iddm + "'";
                        SqlCommand cmd = new SqlCommand(sql, chuoiketnoi.LayKetNoi);
                        int result = cmd.ExecuteNonQuery();

                        int so_dong = grvDanhMuc.PageSize;       //moi trang co bao nhieu dong
                        stt = tranght * so_dong + 1;
                        HienThiDanhMuc();
                    }
                    catch
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Thông báo", "alert('Không thể xóa danh mục! Có sản phẩm tại danh mục này đang được đặt hàng bởi khách hàng.');", true);
                    }
                }
            }
        }
    }
}