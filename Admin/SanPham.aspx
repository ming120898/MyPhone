﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.Master" AutoEventWireup="true" CodeBehind="SanPham.aspx.cs" Inherits="BanDienThoai.Admin.SanPham" %>
<asp:Content ID="Content1" ContentPlaceHolderID="tieude" runat="server">
    <asp:label id="Label1" runat="server" text="SẢN PHẨM"></asp:label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="phanchinh" runat="server">
    <div class="menuright">
        <asp:DropDownList ID="dropDanhMucSP" runat="server" Width="156px" Height="30px"/>
        &nbsp;<asp:Button ID="btnTimKiem" runat="server" CssClass="stylebtn" OnClick="btnTimKiem_Click" style="left: 3px; top: 0px" Text="Tìm kiếm" />
&nbsp;<asp:Button ID="bntThemSanPham" runat="server" Text="Thêm Sản Phẩm" OnClick="bntThemSanPham_Click" CssClass="stylebtn" style="left: -2px; top: 0px" />
        <asp:Button ID="btnXoaSP" runat="server" Text="Xóa" CssClass="stylebtn" style="background-color: red;"  BorderColor="Red" OnClick="btnXoaSP_Click" OnClientClick="return confirm('Bạn có thực sự muốn xóa không?')" />
        <br />
        &nbsp;<br />
        <asp:Label ID="lblThongBao" runat="server" ForeColor="Red"></asp:Label>
    </div>
    <center>
        <asp:GridView ID="dataSanPham" runat="server" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="dataSanPham_PageIndexChanging">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:CheckBox ID="ckbChon" runat="server" />
                </ItemTemplate>
                <HeaderStyle BackColor="#E0E0E0" ForeColor="Maroon" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="STT">
                <ItemTemplate><%# get_stt() %></ItemTemplate>
                <HeaderStyle BackColor="#E0E0E0" ForeColor="Maroon" />
            </asp:TemplateField>
            <asp:ImageField HeaderText="Ảnh" DataImageUrlField="IdHinhSanPham" DataImageUrlFormatString="~/Admin/HinhSanPham.ashx?IdHinhSanPham={0}">
                <ControlStyle Height="50px" />
                <HeaderStyle BackColor="#E0E0E0" ForeColor="Maroon" />
            </asp:ImageField>

            <asp:BoundField HeaderText="ID Sản Phẩm" DataField="IdSanPham">
                <HeaderStyle BackColor="#E0E0E0" ForeColor="Maroon" />
            </asp:BoundField>
            <asp:HyperLinkField DataNavigateUrlFields="IdSanPham" DataNavigateUrlFormatString="SuaSanPham.aspx?IdSanPham={0}" DataTextField="TenSanPham" HeaderText="Tên SP">
                <HeaderStyle BackColor="#E0E0E0" ForeColor="Maroon" />
            </asp:HyperLinkField>
            <asp:BoundField HeaderText="Danh Mục" DataField="TenDanhMucSanPham">
                <HeaderStyle BackColor="#E0E0E0" ForeColor="Maroon" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Giá SP" DataField="GiaSanPham">
                <HeaderStyle BackColor="#E0E0E0" ForeColor="Maroon" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Số lượng" DataField="SoLuong">
                <HeaderStyle BackColor="#E0E0E0" ForeColor="Maroon" />
            </asp:BoundField>
        </Columns>
        <EmptyDataTemplate>
            <asp:CheckBox ID="ckbChon" runat="server" />
        </EmptyDataTemplate>

    </asp:GridView>

    </center>
</asp:Content>
