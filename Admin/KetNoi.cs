﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace BanDienThoai
{
    public class KetNoi
    {
        public string ConnectionString()
        {
            return
            WebConfigurationManager.ConnectionStrings["SQLCONN"].ConnectionString;
            //Tra ve chuoi ket noi voi sql server cua bien SQLCONN. SQLCONN la ten do ta dat.
        }
    }
}