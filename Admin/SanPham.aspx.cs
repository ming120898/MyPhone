﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace BanDienThoai.Admin
{
    public partial class SanPham : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                HienSanPham();
                HienDanhMucSP();
            }
        }

        private void HienDanhMucSP()
        {
            SqlDataSource sqldata = new SqlDataSource();
            try
            {
                KetNoiCSDL chuoiketnoi = new KetNoiCSDL();
                sqldata.ConnectionString = chuoiketnoi.GetSetChuoiKetNoi;
                sqldata.SelectCommandType = SqlDataSourceCommandType.StoredProcedure;
                sqldata.SelectCommand = "DanhMucSanPham_Select";
            }
            catch
            {
                Response.Redirect("../Trangloi.aspx");
            }
            dropDanhMucSP.DataTextField = "TenDanhMucSanPham";
            dropDanhMucSP.DataValueField = "IDDanhMucSanPham";
            dropDanhMucSP.DataSource = sqldata;
            dropDanhMucSP.DataBind();
        }

        private void HienSanPham()
        {
            SqlDataSource sqldata = new SqlDataSource();
            try
            {
                KetNoiCSDL chuoiketnoi = new KetNoiCSDL();
                sqldata.ConnectionString = chuoiketnoi.GetSetChuoiKetNoi;
                sqldata.SelectCommandType = SqlDataSourceCommandType.StoredProcedure;
                sqldata.SelectCommand = "SanPham_Select";
            }
            catch
            {
                Response.Redirect("../Trangloi.aspx");
            }
            dataSanPham.DataSource = sqldata;
            dataSanPham.DataBind();
        }
        int stt = 1;
        protected string get_stt()
        {
            return Convert.ToString(stt++);
        }
        protected void bntThemSanPham_Click(object sender, EventArgs e)
        {
            Response.Redirect("ThemSanPham.aspx");
        }

        int tranght = 1;
        protected void dataSanPham_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dataSanPham.PageIndex = e.NewPageIndex;   //trang hien tai
            tranght = e.NewPageIndex;
            int trang_thu = e.NewPageIndex;      //the hien trang thu may
            int so_dong = dataSanPham.PageSize;       //moi trang co bao nhieu dong
            stt = trang_thu * so_dong + 1;
            HienSanPham();
        }

        protected void btnXoaSP_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in dataSanPham.Rows)
            {
                CheckBox ck = (CheckBox)row.FindControl("ckbChon");
                if (ck != null && ck.Checked)
                {
                    try
                    {
                        int id = int.Parse(row.Cells[3].Text);
                        KetNoiCSDL chuoiketnoi = new KetNoiCSDL();
                        chuoiketnoi.MoKetNoi();
                        string sql = "SELECT IDHinhSanPham FROM SanPham WHERE IDSanPham = '" + id + "'";
                        SqlDataSource data = new SqlDataSource(chuoiketnoi.GetSetChuoiKetNoi, sql);
                        GridView grid = new GridView();
                        grid.DataSource = data;
                        grid.DataBind();
                        int Idhinhsanpham = int.Parse(grid.Rows[0].Cells[0].Text.ToString());

                        string sql1 = "DELETE FROM SanPham WHERE IdSanPham = '" + id + "'; DELETE FROM HinhSanPham WHERE IdHinhSanPham = '" + Idhinhsanpham + "'";
                        SqlCommand TruyVan = new SqlCommand(sql1, chuoiketnoi.LayKetNoi);
                        int SoBanGhi = TruyVan.ExecuteNonQuery();

                        int so_dong = dataSanPham.PageSize;       //moi trang co bao nhieu dong
                        stt = tranght * so_dong + 1;
                        HienSanPham();

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Thông báo", "alert('Xóa sản phẩm thành công.');", true);
                    }
                    catch
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Thông báo", "alert('Không thể xóa sản phẩm! Sản phẩm đang được đặt hàng bởi khách hàng.');", true);
                    }
                }
            }
        }

        protected void btnTimKiem_Click(object sender, EventArgs e)
        {
            SqlDataSource sqldata = new SqlDataSource();
            try
            {
                KetNoiCSDL chuoiketnoi = new KetNoiCSDL();
                sqldata.ConnectionString = chuoiketnoi.GetSetChuoiKetNoi;
                sqldata.SelectCommandType = SqlDataSourceCommandType.StoredProcedure;
                sqldata.SelectCommand = "SanPham_SelectByDanhMuc";
                sqldata.SelectParameters.Add("TenDanhMucSanPham", dropDanhMucSP.SelectedItem.ToString());
            }
            catch
            {
                Response.Redirect("../Trangloi.aspx");
            }
            dataSanPham.DataSource = sqldata;
            dataSanPham.DataBind();
        }
    }
}