﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace BanDienThoai.Admin
{
    public class XuLyDangNhapAdmin
    {
        private NguoiDung _nguoidung;
        public NguoiDung Nguoidung
        {
            get { return _nguoidung; }
            set { _nguoidung = value; }
        }
        private SqlDataSource _ketqua;
        public SqlDataSource Ketqua
        {
            get { return _ketqua; }
            set { _ketqua = value; }
        }
        private bool _dangnhaphople;
        public bool Dangnhaphople
        {
            get { return _dangnhaphople; }
            set { _dangnhaphople = value; }
        }
        public void Thucthi()
        {
            TruyVanDuLieuDangNhapAdmin dangnhapadmin = new
            TruyVanDuLieuDangNhapAdmin();
            dangnhapadmin.Nguoidung = this.Nguoidung;
            Ketqua = dangnhapadmin.Laydulieu();
            GridView grid = new GridView();
            grid.DataSource = Ketqua;
            grid.DataBind();
            if (grid.Rows.Count != 0)
            {
                Dangnhaphople = true;
                Nguoidung.Hoten = grid.Rows[0].Cells[2].Text;
            }
            else
            {
                Dangnhaphople = false;
            }
        }
    }
}