﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BanDienThoai.Admin
{
    public partial class ThongKeDonHang1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                HienTatCaDonHang();
            }
        }
        private void HienTatCaDonHang()
        {
            try
            {
                SqlDataSource sqldata = new SqlDataSource();
                KetNoi chuoiketnoi = new KetNoi();
                sqldata.ConnectionString = chuoiketnoi.ConnectionString();
                sqldata.SelectCommandType = SqlDataSourceCommandType.StoredProcedure;
                sqldata.SelectCommand = "DonHangAll_Select";
                gridTatCaDonHang.DataSource = sqldata;
                gridTatCaDonHang.DataBind();
            }
            catch
            {
                Response.Redirect("../error.html");
            }

        }

        protected void btnImport_Click(object sender, EventArgs e)
        {

        }

        protected void btnXuatFile_Click(object sender, EventArgs e)
        {

        }
    }
}