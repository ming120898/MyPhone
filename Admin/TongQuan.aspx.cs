﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace BanDienThoai.Admin
{
    public partial class TongQuan : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DateTime dt = DateTime.Now;
                int ngay = int.Parse(dt.ToString("dd"));
                int thang = int.Parse(dt.ToString("MM"));
                int nam = int.Parse(dt.ToString("yyyy"));

                Label2.Text = "NGÀY (" + ngay + "/" + thang + "/" + nam + ")";
                Label3.Text = "THÁNG (" + thang + "/" + nam + ")";
                TruyCap();
                TongDonDat(ngay,thang,nam);
                TongSanPham(ngay,thang,nam);
                TongDoanhThu(ngay, thang, nam);
            }
        }

        private void TruyCap()
        {
            KetNoiCSDL chuoiketnoi = new KetNoiCSDL();
            chuoiketnoi.MoKetNoi();
            string sql = "SELECT * FROM ThongKeTruyCap";
            SqlCommand cmd = new SqlCommand(sql, chuoiketnoi.LayKetNoi);
            Label24.Text = cmd.ExecuteScalar().ToString() + " truy cập";
        }
        private void TongDonDat(int ngay, int thang, int nam)
        {
            KetNoiCSDL chuoiketnoi = new KetNoiCSDL();
            chuoiketnoi.MoKetNoi();

            #region Tổng đơn đặt trong ngày
            string sql = "SELECT COUNT(*) FROM DonHang WHERE DAY(NgayTaoDonHang) = "+ngay+" and MONTH(NgayTaoDonHang) = "+thang+" and YEAR(NgayTaoDonHang) = "+nam+"";
            SqlCommand cmd = new SqlCommand(sql, chuoiketnoi.LayKetNoi);
            Label15.Text = cmd.ExecuteScalar().ToString() + " đơn";
            #endregion

            #region Tổng đơn đặt trong tháng
            string sql1 = "SELECT COUNT(*) FROM DonHang WHERE MONTH(NgayTaoDonHang) = " + thang + " and YEAR(NgayTaoDonHang) = " + nam + "";
            SqlCommand cmd1 = new SqlCommand(sql1, chuoiketnoi.LayKetNoi);
            Label20.Text = cmd1.ExecuteScalar().ToString() + " đơn";
            #endregion
        }

        private void TongSanPham(int ngay, int thang, int nam)
        {
            KetNoiCSDL chuoiketnoi = new KetNoiCSDL();
            chuoiketnoi.MoKetNoi();

            #region Tổng sản phẩm đặt trong ngày
            string sql2 = "SELECT SUM(SoLuongSanPham) FROM DonHang,ChiTietDonHang WHERE DAY(NgayTaoDonHang) = "+ngay+" and MONTH(NgayTaoDonHang) = "+thang+" and YEAR(NgayTaoDonHang) = "+nam+" and DonHang.IdDonHang = ChiTietDonHang.IdDonHang";
            SqlCommand cmd2 = new SqlCommand(sql2, chuoiketnoi.LayKetNoi);
            if(cmd2.ExecuteScalar().ToString() != "")
            {
                Label16.Text = cmd2.ExecuteScalar().ToString() + " sản phẩm";
            }
            else
            {
                Label16.Text = "0" + " sản phẩm";
            }
            #endregion

            #region Tổng sản phẩm đặt trong tháng
            string sql3 = "SELECT SUM(SoLuongSanPham) FROM DonHang,ChiTietDonHang WHERE MONTH(NgayTaoDonHang) = " + thang + " and YEAR(NgayTaoDonHang) = " + nam + " and DonHang.IdDonHang = ChiTietDonHang.IdDonHang";
            SqlCommand cmd3 = new SqlCommand(sql3, chuoiketnoi.LayKetNoi);
            if (cmd3.ExecuteScalar().ToString() != "")
            {
                Label21.Text = cmd3.ExecuteScalar().ToString() + " sản phẩm";
            }
            else
            {
                Label21.Text = "0" + " sản phẩm";
            }
            #endregion
        }

        private void TongDoanhThu(int ngay, int thang, int nam)
        {
            KetNoiCSDL chuoiketnoi = new KetNoiCSDL();
            chuoiketnoi.MoKetNoi();

            #region Tổng doanh thu trong ngày
            string sql4 = "SELECT SUM(GiaSanPham*SoLuongSanPham) FROM DonHang, ChiTietDonHang, SanPham WHERE DAY(NgayTaoDonHang) = " + ngay + " and MONTH(NgayTaoDonHang) = " + thang + " and YEAR(NgayTaoDonHang) = " + nam + " and DonHang.IdDonHang = ChiTietDonHang.IdDonHang and ChiTietDonHang.IdSanPham = SanPham.IdSanPham";
            SqlCommand cmd4 = new SqlCommand(sql4, chuoiketnoi.LayKetNoi);
            if (cmd4.ExecuteScalar().ToString() != "")
            {
                Label18.Text = cmd4.ExecuteScalar().ToString() + " VNĐ";
            }
            else
            {
                Label18.Text = "0 VNĐ";
            }
            #endregion

            #region Tổng doanh thu trong tháng
            string sql5 = "SELECT SUM(GiaSanPham*SoLuongSanPham) FROM DonHang, ChiTietDonHang, SanPham WHERE MONTH(NgayTaoDonHang) = " + thang + " and YEAR(NgayTaoDonHang) = " + nam + " and DonHang.IdDonHang = ChiTietDonHang.IdDonHang and ChiTietDonHang.IdSanPham = SanPham.IdSanPham";
            SqlCommand cmd5 = new SqlCommand(sql5, chuoiketnoi.LayKetNoi);
            if (cmd5.ExecuteScalar().ToString() != "")
            {
                Label22.Text = cmd5.ExecuteScalar().ToString() + " VNĐ";
            }
            else
            {
                Label22.Text = "0 VNĐ";
            }
            #endregion
        }
    }
}