﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace BanDienThoai.Admin
{
    public partial class ThemDanhMuc : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnThem_Click(object sender, EventArgs e)
        {
            lblthongbao.Text = "";
            if (IsValid)
            {
                try
                {
                    KetNoiCSDL chuoiketnoi = new KetNoiCSDL();
                    SqlConnection conect = new SqlConnection(chuoiketnoi.GetSetChuoiKetNoi);
                    conect.Open();
                    SqlCommand com = new SqlCommand();
                    com.Connection = conect;
                    com.CommandType = CommandType.StoredProcedure;
                    com.CommandText = "DanhMucSanPham_Insert";
                    com.Parameters.Add("@TenDanhMucSanPham", SqlDbType.NVarChar).Value = txtTenDanhMuc.Text;
                    com.ExecuteNonQuery();

                    lblthongbao.Text = "Thêm thành công.";
                }
                catch
                {
                    Response.Redirect("../Trangloi.aspx");
                }
            }
        }

        protected void btnTroVe_Click(object sender, EventArgs e)
        {
            Response.Redirect("DanhMuc.aspx");
        }
    }
}