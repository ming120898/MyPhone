﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace BanDienThoai.Admin
{
    public partial class SuaSanPham : System.Web.UI.Page
    {
        private const string IDHinh = " ";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["Idsanpham"] == null)
                Response.Redirect("SanPham.aspx");
            if (!IsPostBack)
            {
                txtTenSanPham.Focus();
                HienDanhMucSanPham();
                HienSanPham();
            }
        }
        //-----------Hiện danh mục sản phẩm----------------
        private void HienDanhMucSanPham()
        {
            SqlDataSource sqldata = new SqlDataSource();
            try
            {
                KetNoiCSDL chuoiketnoi = new KetNoiCSDL();
                sqldata.ConnectionString = chuoiketnoi.GetSetChuoiKetNoi;
                sqldata.SelectCommandType = SqlDataSourceCommandType.StoredProcedure;
                sqldata.SelectCommand = "DanhMucSanPham_Select";
            }
            catch
            {
                Response.Redirect("../Trangloi.aspx");
            }
            dropDanhMucSanPham.DataTextField = "TenDanhMucSanPham";
            dropDanhMucSanPham.DataValueField = "IDDanhMucSanPham";
            dropDanhMucSanPham.DataSource = sqldata;
            dropDanhMucSanPham.DataBind();
        }
        //----------Hiện sản phẩm theo id sản phẩm--------------
        private void HienSanPham()
        {
            SP Sanpham = new SP();

            SqlDataSource sqldata = new SqlDataSource();
            KetNoiCSDL chuoiketnoi = new KetNoiCSDL();
            sqldata.ConnectionString = chuoiketnoi.GetSetChuoiKetNoi;
            sqldata.SelectCommandType = SqlDataSourceCommandType.StoredProcedure;
            sqldata.SelectCommand = "SanPhamByID_Select";
            sqldata.SelectParameters.Add("IdSanPham", Request.QueryString["Idsanpham"].ToString());

            GridView grid = new GridView();
            grid.DataSource = sqldata;
            grid.DataBind();
            Sanpham.Ten = grid.Rows[0].Cells[1].Text.ToString();
            Sanpham.Mota = grid.Rows[0].Cells[4].Text.ToString();
            Sanpham.Giasanpham = Convert.ToInt32(grid.Rows[0].Cells[5].Text.ToString());
            Sanpham.Idsanpham = int.Parse(grid.Rows[0].Cells[0].Text.ToString());
            Sanpham.Danhmucsanpham.Tendanhmucsanpham = grid.Rows[0].Cells[2].Text.ToString();
            Sanpham.Idhinhsanpham = int.Parse(grid.Rows[0].Cells[3].Text.ToString());
            Sanpham.Soluong = int.Parse(grid.Rows[0].Cells[6].Text.ToString());

            try
            {
                txtTenSanPham.Text = Sanpham.Ten;
                CKEditorControlMoTa.Text = Sanpham.Mota;
                txtGia.Text = Sanpham.Giasanpham.ToString();
                imgHinhSanPham.ImageUrl = "HinhSanPham.ashx?Idhinhsanpham=" +Sanpham.Idhinhsanpham.ToString();
                dropDanhMucSanPham.SelectedIndex =
                dropDanhMucSanPham.Items.IndexOf(dropDanhMucSanPham.Items.FindByText(Sanpham.Danhmucsanpham.Tendanhmucsanpham));
                LuuTamIdHinhSanPham = Sanpham.Idhinhsanpham;
                txtSL.Text = Sanpham.Soluong.ToString();
            }
            catch
            {
                Response.Redirect("../Trangloi.aspx");
            }
        }
        //---------------Cập nhật thay đổi sản phẩm--------------------
        protected void btnCapNhat_Click(object sender, EventArgs e)
        {
            KetNoiCSDL chuoiketnoi = new KetNoiCSDL();
            if (IsValid)
            {
                int Idsanpham = int.Parse(Request.QueryString["IDsanpham"]);
                SP Spham = new SP();

                Spham.Ten = txtTenSanPham.Text;
                Spham.Mota = CKEditorControlMoTa.Text;
                Spham.Giasanpham = Convert.ToDecimal(txtGia.Text);
                Spham.Soluong = int.Parse(txtSL.Text);
                Spham.Iddanhmucsanpham = int.Parse(
                dropDanhMucSanPham.SelectedItem.Value);
                Spham.Idhinhsanpham = LuuTamIdHinhSanPham;
                if (fileuploadHinhSanPham.HasFile)
                {
                    Spham.Dulieuhinhsanpham = fileuploadHinhSanPham.FileBytes;
                }
                else
                {
                    SqlDataSource sqldata = new SqlDataSource();
                    sqldata.ConnectionString = chuoiketnoi.GetSetChuoiKetNoi;
                    sqldata.SelectCommandType = SqlDataSourceCommandType.StoredProcedure;
                    sqldata.SelectCommand = "HinhSanpham_Select";
                    sqldata.SelectParameters.Add("IDHinhSanPham", Spham.Idhinhsanpham.ToString());
                    DataView view = (DataView)sqldata.Select(DataSourceSelectArguments.Empty);
                    Spham.Dulieuhinhsanpham = (byte[])view[0]["DuLieuHinhSanPham"];
                }
                
                try
                {
                    SqlConnection conect = new SqlConnection(chuoiketnoi.GetSetChuoiKetNoi);
                    conect.Open();
                    SqlCommand com = new SqlCommand();
                    com.Connection = conect;
                    com.CommandType = CommandType.StoredProcedure;
                    com.CommandText = "Sanpham_UpDate";
                    com.Parameters.Add("@IdDanhMucSanPham", SqlDbType.Int).Value = Spham.Iddanhmucsanpham;
                    com.Parameters.Add("@TenSanPham", SqlDbType.NVarChar).Value = Spham.Ten;
                    com.Parameters.Add("@IdHinhSanPham", SqlDbType.Int).Value = Spham.Idhinhsanpham;
                    com.Parameters.Add("@DuLieuHinhSanPham", SqlDbType.Image).Value = Spham.Dulieuhinhsanpham;
                    com.Parameters.Add("@MoTaSanPham", SqlDbType.NText).Value = Spham.Mota;
                    com.Parameters.Add("@GiaSanPham", SqlDbType.Int).Value = Spham.Giasanpham;
                    com.Parameters.Add("@SoLuong", SqlDbType.Int).Value = Spham.Soluong;
                    com.Parameters.Add("@IdSanPham ", SqlDbType.Int).Value = Idsanpham;
                    com.ExecuteNonQuery();
                }
                catch
                {
                    Response.Redirect("../Trangloi.aspx");
                }
                Response.Redirect("SanPham.aspx");
            }
        }
        
        // Lưu hình để lấy lại hình sản phẩm trong trường hợp hình không thay đổi
        private int LuuTamIdHinhSanPham
        {
            get { return (int)ViewState[IDHinh]; }
            set { ViewState[IDHinh] = value; }
        }
        //---------sự kiện nút bỏ qua----------------------
        protected void btnTroVe_Click(object sender, EventArgs e)
        {
            Response.Redirect("SanPham.aspx");
        }
    }
}