﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.Master" AutoEventWireup="true" CodeBehind="SuaDanhMuc.aspx.cs" Inherits="BanDienThoai.Admin.SuaDanhMuc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="tieude" runat="server">
    <asp:label id="Label1" runat="server" text="CẬP NHẬT DANH MỤC"></asp:label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="phanchinh" runat="server">
    <center>
        <table style="width:100%;">
            <tr>
                <td>
        <asp:Label ID="Label2" runat="server" Text="Tên danh muc:"></asp:Label>
        &nbsp;
                    <asp:TextBox ID="txtTenDanhMuc" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnCapNhat" runat="server" CssClass="stylebtn" OnClick="btnCapNhat_Click" Text="Cập nhật" />
&nbsp;
                    <asp:Button ID="btnTroVe" runat="server" BackColor="Red" BorderColor="Red" CausesValidation="False" CssClass="stylebtn" OnClick="btnTroVe_Click" style="left: 0px; top: 0px" Text="Trở về" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTenDanhMuc" ErrorMessage="Tên danh mục không được bỏ trống" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
        <asp:Label ID="lblthongbao" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
    </center>
</asp:Content>
