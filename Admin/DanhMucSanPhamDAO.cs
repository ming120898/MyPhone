﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

namespace BanDienThoai
{
    public class DanhMucSanPhamDAO
    {
        public SqlDataSource Laydulieu()
        {
            KetNoi chuoiketnoi = new KetNoi();
            string sql = "SELECT IDDanhMucSanPham, TenDanhMucSanPham FROM DanhMucSanPham";
            SqlDataSource data = new SqlDataSource(chuoiketnoi.ConnectionString(), sql);
            return data;
        }
        public bool XoaDanhMuc(int iddm)
        {
            KetNoi chuoiketnoi = new KetNoi();
            SqlConnection cnn = new SqlConnection(chuoiketnoi.ConnectionString());
            cnn.Open();
            string sql = "DELETE FROM SanPham WHERE IdDanhMucSanPham = '" + iddm + "' ; DELETE FROM DanhMucSanPham WHERE IdDanhMucSanPham = '" + iddm + "'";
            SqlCommand cmd = new SqlCommand(sql, cnn);
            int result = (int)cmd.ExecuteNonQuery();
            return (result >= 1);
        }
        public bool ThemDanhMuc(string tendanhmuc)
        {
            KetNoi chuoiketnoi = new KetNoi();
            SqlConnection cnn = new SqlConnection(chuoiketnoi.ConnectionString());
            cnn.Open();
            string sql = "INSERT INTO DanhMucSanPham(TenDanhMucSanPham) values(@TenDanhMucSanPham)";
            SqlCommand cmd = new SqlCommand(sql, cnn);
            cmd.Parameters.AddWithValue("@TenDanhMucSanPham", tendanhmuc);
            int result = (int)cmd.ExecuteNonQuery();
            return (result >= 1);
        }
        public bool SuaDanhMuc(int iddm, string tendanhmuc)
        {
            KetNoi chuoiketnoi = new KetNoi();
            SqlConnection cnn = new SqlConnection(chuoiketnoi.ConnectionString());
            cnn.Open();
            string sql = "UPDATE DanhMucSanPham SET TenDanhMucSanPham = @TenDanhMucSanPham WHERE IdDanhMucSanPham = @IdDanhMucSanPham" ;
            SqlCommand cmd = new SqlCommand(sql, cnn);
            cmd.Parameters.AddWithValue("@TenDanhMucSanPham", tendanhmuc);
            cmd.Parameters.AddWithValue("@IdDanhMucSanPham", iddm);
            int result = (int)cmd.ExecuteNonQuery();
            return (result >= 1);
        }
    }
}