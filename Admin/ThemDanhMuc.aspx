﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.Master" AutoEventWireup="true" CodeBehind="ThemDanhMuc.aspx.cs" Inherits="BanDienThoai.Admin.ThemDanhMuc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="tieude" runat="server">
    <asp:label id="Label1" runat="server" text="THÊM DANH MỤC"></asp:label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="phanchinh" runat="server">
    <center>
        <asp:Label ID="Label2" runat="server" Text="Tên danh muc:"></asp:Label>
        &nbsp;
        <asp:TextBox ID="txtTenDanhMuc" runat="server"></asp:TextBox>
        &nbsp;
        <br />
        <br />
        <asp:Button ID="btnThem" runat="server" OnClick="btnThem_Click" Text="Thêm" CssClass="stylebtn"/>
        <asp:Button ID="btnTroVe" runat="server" Text="Trở về" OnClick="btnTroVe_Click" CausesValidation="False" CssClass="stylebtn" style="background-color: red;" BorderColor="Red" />
        <br />
        <br />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTenDanhMuc" ErrorMessage="Tên danh mục không được bỏ trống" ForeColor="Red"></asp:RequiredFieldValidator>
        <br />
        <asp:Label ID="lblthongbao" runat="server" ForeColor="Red"></asp:Label>
    </center>
</asp:Content>
