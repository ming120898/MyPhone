﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPhu.Master" AutoEventWireup="true" CodeBehind="ThongTinCaNhan.aspx.cs" Inherits="BanDienThoai.ThongTinCaNhan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:GridView ID="dgvKetQua" runat="server" AutoGenerateColumns="False" Width="516px" DataKeyNames="TenDangNhap">
        <Columns>
            <asp:TemplateField HeaderText="Họ Tên">
                <ItemTemplate>
                    <asp:TextBox ID="textHoTen" runat="server" Text='<%# Eval("HoTen") %>' Width="93px"></asp:TextBox>
                </ItemTemplate>
                <HeaderStyle BackColor="#E0E0E0" BorderColor="#404040" ForeColor="Maroon" HorizontalAlign="Center"
                    VerticalAlign="Middle" BorderStyle="Solid" BorderWidth="1px" />
                <ItemStyle ForeColor="#404040" HorizontalAlign="Center" VerticalAlign="Middle" BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Địa Chỉ">
                <ItemTemplate>
                    <asp:TextBox ID="textDiaChi" runat="server" Text='<%# Eval("DiaChi") %>' Width="93px"></asp:TextBox>
                </ItemTemplate>
                <HeaderStyle BackColor="#E0E0E0" BorderColor="#404040" ForeColor="Maroon" HorizontalAlign="Center"
                    VerticalAlign="Middle" BorderStyle="Solid" BorderWidth="1px" />
                <ItemStyle ForeColor="#404040" HorizontalAlign="Center" VerticalAlign="Middle" BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Email"> 
                <ItemTemplate>
                    <asp:TextBox ID="textEmail" runat="server" Text='<%# Eval("Email") %>' Width="93px"></asp:TextBox>
                </ItemTemplate>
                <HeaderStyle BackColor="#E0E0E0" BorderColor="#404040" ForeColor="Maroon" HorizontalAlign="Center"
                    VerticalAlign="Middle" BorderStyle="Solid" BorderWidth="1px" />
                <ItemStyle ForeColor="#404040" HorizontalAlign="Right" VerticalAlign="Middle" BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Số Điện Thoại">
                <ItemTemplate>
                    <asp:TextBox ID="textSDT" runat="server" Text='<%# Eval("SoDienThoai") %>' Width="93px"></asp:TextBox>
                </ItemTemplate>
                <HeaderStyle BackColor="#E0E0E0" BorderColor="#404040" ForeColor="Maroon" HorizontalAlign="Center"
                    VerticalAlign="Middle" BorderStyle="Solid" BorderWidth="1px" />
                <ItemStyle ForeColor="#404040" HorizontalAlign="Right" VerticalAlign="Middle" BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Số Fax">
               <ItemTemplate>
                    <asp:TextBox ID="textSoFax" runat="server" Text='<%# Eval("SoFax") %>' Width="93px"></asp:TextBox>
                </ItemTemplate>
                <HeaderStyle BackColor="#E0E0E0" BorderColor="#404040" ForeColor="Maroon" HorizontalAlign="Center"
                    VerticalAlign="Middle" BorderStyle="Solid" BorderWidth="1px" />
                <ItemStyle ForeColor="#404040" HorizontalAlign="Center" VerticalAlign="Middle" BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:button runat="server" text="Đổi Mật Khẩu" OnClick="Unnamed1_Click1" />
</asp:Content>
