﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
namespace BanDienThoai
{
    public partial class DangKy : System.Web.UI.Page
    {
        //Lớp Kết Nối CSDL
        KetNoiCSDL KN = new KetNoiCSDL();
        GridView dgvKetQua = new GridView();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["ThongBaoDangKy"] != null)
            {
                Response.Write(Session["ThongBaoDangKy"]);
                Session.Remove("ThongBaoDangKy");
            }
            textHoTen.Focus();
        }

        protected void btnDangKy_Click(object sender, ImageClickEventArgs e)
        {
            NguoiDung nguoidung = new NguoiDung();
            //XuLyThemNguoiDung themnguoidung = new XuLyThemNguoiDung();
            if (IsValid)
            {
                    KN.MoKetNoi();
                    string Lenh = "SELECT COUNT(*) FROM NguoiDung where TenDangNhap = '" + textTenDangNhap.Text + "'";
                    SqlCommand cmd = new SqlCommand(Lenh, KN.LayKetNoi); 
                    if (cmd.ExecuteScalar().ToString() == "0") {
                        try
                        {
                            SqlDataSource sqldata = new SqlDataSource();
                            KetNoiCSDL chuoiketnoi = new KetNoiCSDL();
                            chuoiketnoi.MoKetNoi();
                            sqldata.ConnectionString = chuoiketnoi.GetSetChuoiKetNoi;
                            sqldata.InsertCommandType = SqlDataSourceCommandType.StoredProcedure;
                            sqldata.InsertCommand = "NguoiDung_Insert";
                            sqldata.InsertParameters.Add("HoTen", textHoTen.Text);
                            sqldata.InsertParameters.Add("TenDangNhap", textTenDangNhap.Text);
                            sqldata.InsertParameters.Add("DiaChi", textQuanHuyen.Text);
                            sqldata.InsertParameters.Add("MaDienThoai", textMaDienThoai.Text);
                            sqldata.InsertParameters.Add("SoDienThoai", textSoDienThoai.Text);
                            sqldata.InsertParameters.Add("SoFax", textFax.Text);
                            sqldata.InsertParameters.Add("Email", textEmail.Text);
                            sqldata.InsertParameters.Add("IDKieuNguoiDung", "1");
                            sqldata.InsertParameters.Add("MatKhau", textMatKhau.Text);
                            sqldata.Insert();
                            KN.DongKetNoi();
                        }
                        catch
                        {
                            Response.Redirect("TrangLoi.aspx");
                        }
                    }
                    else
                    {
                        Session["ThongBaoDangKy"] = "<script>alert('Trùng Tên Đăng Nhập vui lòng chọn Tên Khác !')</script>";
                        Response.Redirect("DangKy.aspx");
                    }
                    Response.Redirect("DangNhap.aspx");
            }
        }
    }
}