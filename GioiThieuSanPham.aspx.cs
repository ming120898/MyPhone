﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BanDienThoai
{
    public partial class GioiThieuSanPham : System.Web.UI.Page
    {
        KetNoiCSDL KetNoi = new KetNoiCSDL();
        //Phương Thức Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                HienThiSanPham();
            }
        }

        //Nút Tìm Kiếm Sản Phẩm
        protected void ImageButtonTim_Click(object sender, ImageClickEventArgs e)
        {
            TimSanPham(textSearch.Text);
        }

        //-----------------------------------Các Hàm Cho Trang Này--------------------------------------//

        //Phương thức Hiển thị sản phẩm
        private void HienThiSanPham()
        {
            dtlSanPham.DataSource = KetNoi.Laydulieu("SanPham_Select");
            dtlSanPham.DataBind();
        }

        //Phương Thức Tìm sản phẩm
        private void TimSanPham(string ChuoiTimKiem)
        {
            dtlSanPham.DataSource = KetNoi.Laydulieu(ChuoiTimKiem, "SanPham_SelectSearch", "tieuchuantim");
            dtlSanPham.DataBind();
            if (dtlSanPham.Items.Count != 0)
                lblketqua.Text = "Có " + dtlSanPham.Items.Count + " sản phẩm được tìm thấy";
            else
            {
                lblketqua.Text = "Không tìm thấy sản phẩm";
            }
        }
    }
}