﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data.SqlClient;
namespace BanDienThoai
{
    public partial class GioHang : System.Web.UI.Page
    {
        KetNoiCSDL KetNoi = new KetNoiCSDL();
        private int _tongtien;
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (Session["ThongBaoHetHang"] != null) {
                Response.Write(Session["ThongBaoHetHang"]);
                Session.Remove("ThongBaoHetHang");
            }
            if (gridgiohang.Rows.Count < 0)
            {
                lblThongBao.Text = "Giỏ Hàng Hiện Chưa Có Sản Phẩm Nào !";
            }
            else
            {
                if (Application["DangNhap"].ToString() == "1")
                {
                    lblThongBao.Text = "Xin chào, " + Application["TenNguoDung"];
                }
            }
            gridgiohang.PageSize = 10;
            if (!IsPostBack)
            {
                
                HienThiGioHang();
            }
            //Session["XoaGioHang"] = 1; đoạn này chỉ chạy khi bấm nút gửi đơn hàng bên trang ThemDonHang
            // dùng để xóa all sp trong giỏ hàng vì gửi đơn hàng rồi mà
            if (Session["XoaGioHang"] != null)
            {
                foreach (GridViewRow row in gridgiohang.Rows)
                {
                    DataKey data = gridgiohang.DataKeys[row.DataItemIndex];//lay du lieu cua cot lam khoa
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        Delete(int.Parse(data.Values["IDgiohang"].ToString()));
                    }
                }
                Session.Remove("XoaGioHang");
                Response.Redirect("GioHang.aspx");
            }
        }
        private void HienThiGioHang()
        {
            gridgiohang.DataSource = KetNoi.Laydulieu(CartGUID(), "GioHang_Select", "CartGUID");
            gridgiohang.DataBind();
        }
        private string CartGUID()
        {
            if (HttpContext.Current.Request.Cookies["DIENTHOAI"] != null)
            {
                return
                HttpContext.Current.Request.Cookies["DIENTHOAI"]["CartID"].ToString();
            }
            else
            {
                Guid CartGUID = Guid.NewGuid();
                HttpCookie cookie = new HttpCookie("DIENTHOAI");
                cookie.Values.Add("CartID", CartGUID.ToString());
                cookie.Expires = DateTime.Now.AddDays(30);
                HttpContext.Current.Response.AppendCookie(cookie);
                return CartGUID.ToString();
            }
        }
        protected void gridgiohang_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                _tongtien += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "ThanhTien"));
            }
            lblTotal.Text = string.Format(_tongtien.ToString()) + " VND";

        }
        protected void ImageButtontieptucmuahang_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("gioithieusanpham.aspx");
        }
        protected void ImageButtonXacnhanthanhtoan_Click(object sender, ImageClickEventArgs e)
        {
            if(Application["DangNhap"].ToString() == "1")
            {
                Response.Redirect("ThemDonHang.aspx");
            }
            else
            {
                Response.Redirect("DangNhap.aspx");
            }
        }
	    protected void ImageButtoncapnhatthaydoi_Click(object sender, ImageClickEventArgs e)
        {
            foreach (GridViewRow row in gridgiohang.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    DataKey data = gridgiohang.DataKeys[row.DataItemIndex];//lay du lieu cua cot lam khoa
                    CheckBox check = (CheckBox)row.FindControl("checkboxDelete");
                    if (check.Checked)
                    {
                        
                        Delete(int.Parse(data.Values["IDgiohang"].ToString()));
                      
                        //IDgiohang la gia tri cua thuoc tinh DataKeyNames="IDgiohang" trong gridview
                        // ma ta tao trong file giao dien giohang.aspx
                    }

                    //-------------------Cập nhật thay đổi số lượng sản phẩm trong TextBox--------------------
                    TextBox textmoi = (TextBox)row.FindControl("textQuantity");
                    int giatri_moi_trong_textbox = int.Parse(textmoi.Text);
                    int giatri_bandau_trong_textbox =
                    int.Parse(gridgiohang.DataKeys[row.DataItemIndex].Value.ToString());
                    try
                    {
                        KetNoiCSDL KetNoi = new KetNoiCSDL();
                        KetNoi.MoKetNoi();
                        string sql = "Select SoLuong from SanPham Where Idsanpham = (Select IdSanPham from GioHang Where IdGioHang = " + gridgiohang.DataKeys[row.DataItemIndex].Value.ToString() + ")";
                        SqlCommand cmd = new SqlCommand(sql, KetNoi.LayKetNoi);
                        int SoLuongTrongKho = (int)cmd.ExecuteScalar();
                        if (giatri_moi_trong_textbox != giatri_bandau_trong_textbox && giatri_moi_trong_textbox <= SoLuongTrongKho)
                        {
                            Update(int.Parse(data.Values["IDgiohang"].ToString()), giatri_moi_trong_textbox);
                        }
                        else
                        {
                            sql = "Select TenSanPham from SanPham Where Idsanpham = (Select IdSanPham from GioHang Where IdGioHang = " + gridgiohang.DataKeys[row.DataItemIndex].Value.ToString() + ")";
                            cmd = new SqlCommand(sql, KetNoi.LayKetNoi);
                            string TenSanPham = (string)cmd.ExecuteScalar();
                            Session["ThongBaoCapNhatSoLuong"] = "<script>alert('Sản Phẩm " + TenSanPham + " Chỉ còn " + SoLuongTrongKho + " Sản Phẩm !')</script>";
                        }
                    }
                    catch { }
                }
            }
            HienThiGioHang();

            int Dem = gridgiohang.Rows.Count;
            if (Dem == 0)
            {
                lblTotal.Text = "0 VND";
                lblThongBao.Text = "Bạn chưa có sản phẩm nào trong giỏ hàng";
            }
            if (Session["ThongBaoCapNhatSoLuong"] != null)
            {
                Response.Write(Session["ThongBaoCapNhatSoLuong"]);
                Session.Remove("ThongBaoCapNhatSoLuong");
            }
        }
        //------------Thủ tục Update------------------
        private void Update(int id, int soluong)
        {
            SqlDataSource sqldata = new SqlDataSource();
            KetNoiCSDL chuoiketnoi = new KetNoiCSDL();
            sqldata.ConnectionString = chuoiketnoi.GetSetChuoiKetNoi;
            sqldata.UpdateCommandType = SqlDataSourceCommandType.StoredProcedure;
            sqldata.UpdateCommand = "GioHang_Update";
            sqldata.UpdateParameters.Add("SoLuong", soluong.ToString());
            sqldata.UpdateParameters.Add("IDGioHang", id.ToString());
            sqldata.Update();
        }
        //--------------------Thủ tục Delete()------------------
        private void Delete(int id)
        {
            int Idgiohang = id;
            SqlDataSource sqldata = new SqlDataSource();
            KetNoiCSDL chuoiketnoi = new KetNoiCSDL();
            sqldata.ConnectionString = chuoiketnoi.GetSetChuoiKetNoi;
            sqldata.DeleteCommandType = SqlDataSourceCommandType.StoredProcedure;
            sqldata.DeleteCommand = "GioHang_Delete";
            sqldata.DeleteParameters.Add("IDGioHang", Idgiohang.ToString());
            sqldata.Delete();
        }
   

    }
}