﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPhu.Master" AutoEventWireup="true" CodeBehind="ChiTietSanPham.aspx.cs" Inherits="BanDienThoai.ChiTietSanPham" %>
<%@ Register TagPrefix="cc1" Namespace="SiteUtils" Assembly="CollectionPager" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:DataList ID="dtlChiTietSanPham" runat="server">
        <ItemTemplate>
            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;margin-top: 15px;">
                <tr>
                    <td rowspan="3">
                        <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("IdHinhSanPham","HienThiHinhSanPham.ashx?IdHinhSanPham={0}") %>' style="width:175px;margin-left: 50px;" />
                    </td>
                    <td rowspan="1">
                        <asp:Label ID="lblTenSanpham" runat="server" Text='<%# Eval("TenSanPham") %>' style="color:#330000;font-weight:bold;font-size: 20px;"></asp:Label>
                        <%--<span style="color: #660000">
                        </span>--%>
                        </br>   
                        <p style="font-size: 5px;"></p>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("GiaSanPham", "{0:0,000,000} VND") %>' style="color:#990000;font-size: 16px;"></asp:Label>                  
                    </td>
                </tr>
                <%--<tr>
                    <td style="width: 350px;font-size: 14px;">
                        <span style="color: #660000">
                        </span>
                        <asp:Label ID="lblGiaSanpham" runat="server" Text='<%# Eval("GiaSanPham", "{0:0,000,000} VND") %>' ForeColor="#990000"></asp:Label></td>
                </tr>--%>
                <tr>
                    <td style="width: 350px; height: 44px;" rowspan="">
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# "GioiThieuSanPham.aspx" %>' ImageUrl="~/images/button_back.jpg" Width="80px">Trở về</asp:HyperLink>
                        <asp:HyperLink ID="HyperLink2" runat="server" Width="80px" ImageUrl="~/images/button_buy.gif" NavigateUrl='<%# Eval("IdSanPham","ThemGioHang.aspx?IDSanpham={0}") %>'>Thêm vào giỏ hàng</asp:HyperLink></td>
                </tr>
                <tr>
                </tr>
                <tr>
                    <td align="center" colspan="2" rowspan="1" style="padding-top: 30px;">
                        <span style="color: #660000;font-size: 16px;"><b>THÔNG TIN CHI TIẾT</b></span></td>
                </tr>
                <tr>
                    <td colspan="2" rowspan="1" align="left">
                        <span style="color: #660000">
                        </span>
                        <asp:Label ID="lblMoTaChiTiet" runat="server" Text='<%# Eval("MoTaSanPham") %>'></asp:Label></td>
                </tr>
            </table>
        </ItemTemplate>
    </asp:DataList>
    <cc1:CollectionPager ID="PhanTrang" runat="server"></cc1:CollectionPager>
</asp:Content>


