﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BanDienThoai
{
    public partial class DonKhachHang : System.Web.UI.Page
    {
        SqlDataSource sqldata;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Label lblWelcome = (Label)Master.FindControl("lblchao");
                if(Application["DangNhap"].ToString() == "1")
                {
                    lblWelcome.Text = "Xin chào, " + Application["TenNguoDung"];
                    HienThiDonHang();
                }
                else
                {
                    lblWelcome.Text = "Hiện chưa có đơn hàng !";
                }
                
            }
        }
        private void HienThiDonHang()
        {
           
            try
            {
                sqldata = new SqlDataSource();
                KetNoiCSDL chuoiketnoi = new KetNoiCSDL();
                sqldata.ConnectionString = chuoiketnoi.GetSetChuoiKetNoi;
                sqldata.SelectCommandType = SqlDataSourceCommandType.StoredProcedure;
                sqldata.SelectCommand = "DonHang_Select";
                sqldata.SelectParameters.Add("IdNguoiDung", Application["IdNguoDung"].ToString());
            }
            catch
            {
                Response.Redirect("Trangloi.aspx");
            }
            gridviewOrders.DataSource = sqldata;
            gridviewOrders.DataBind();
        }
        protected void ImageButtontrove_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("GioiThieuSanPham.aspx");

        }
    }
}